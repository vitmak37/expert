<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Services\SafecrowService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

//Route::group(['as'=>'oauth.','prefix'=>'oauth'],function(){
//  Route::get('/login','Auth\OAuthController@login')->name('login');
//  Route::get('/callback','Auth\OAuthController@callback')->name('callback');
//  Route::get('/user','Auth\OAuthController@getUser')->name('user');
//});
//
//Auth::routes();
//Route::get('logout', 'Auth\LoginController@logout')->name('logout.link');
//
//Route::get('orders/personal', 'OrderController@personal')->name('orders.personal');
//Route::get('orders', 'OrderController@index')->name('orders.index');
//
//Route::get('orders/{order}/decline', 'OrderController@decline')->name('orders.decline');
//Route::get('orders/{order}/done', 'OrderController@done')->name('orders.done');
//Route::get('orders/{order}/resume', 'OrderController@resume')->name('orders.resume');
//Route::get('orders/{order}/argue', 'OrderController@argue')->name('orders.argue');
//
//Route::resource('orders', 'OrderController')->only(['index', 'show', 'create', 'store']);
//Route::post('responds/{respond}/comment', 'RespondController@comment')->name('responds.comment');
//Route::post('responds', 'RespondController@store')->name('responds.store')->middleware(['auth']);
//Route::post('responds/{respond}/choose', 'RespondController@choose')->name('responds.choose');
//Route::post('complaint', 'ComplaintController@store')->name('complaint.store')->middleware(['auth']);
//
//Route::resource('users', 'UserController')->only(['show']);
//Route::get('profile', 'UserController@profile')->name('users.profile')->middleware(['auth']);
//Route::post('profile', 'UserController@update')->name('profile.update')->middleware(['auth']);
//Route::put('profile', 'UserController@setType')->name('users.profile.set')->middleware((['auth']));
//
//
//// STATIC PAGES
//
//Route::get('about', 'StaticController@about')->name('about');
//Route::get('ru1es', 'StaticController@rules')->name('rules');
//Route::get('faq', 'StaticController@faq')->name('faq');
//
//// Layout
//
//Route::get('/', function () {
//    return view('index');
//});

Route::middleware('auth')->group(function () {
    Route::get('profile', 'UserController@profile')->name('profile');
    Route::put('profile', 'UserController@setType')->name('users.profile.set');
    Route::post('profile', 'UserController@update')->name('profile.update');
    Route::post('profile/pw', 'UserController@updatepw')->name('users.updatepw');
    Route::post('profile/photo', 'UserController@uploadPhoto')->name('profile.upload');

    Route::get('orders/create', 'OrderController@create')->name('orders.create');
    Route::post('orders', 'OrderController@store')->name('orders.store');
    Route::get('orders/{order}/done', 'OrderController@done')->name('orders.done');
    Route::get('orders/{order}/resume', 'OrderController@resume')->name('orders.resume');
    Route::get('orders/{order}/argue', 'OrderController@argue')->name('orders.argue');
    Route::get('orders/{order}/update', 'OrderController@updateStatus')->name('orders.updateStatus');
    Route::post('complaint', 'ComplaintController@store')->name('complaint.store');
    Route::get('orders/{order}/decline', 'OrderController@decline')->name('orders.decline');
    Route::get('orders/{order}/close', 'OrderController@close')->name('orders.close');
    Route::get('orders/{order}/renew', 'OrderController@guarantee')->name('orders.renew');

    Route::post('responds', 'RespondController@store')->name('responds.store')->middleware('sccard');
    Route::post('responds/{respond}/choose', 'RespondController@choose')->name('responds.choose');
    Route::post('responds/{id}/updatecost', 'RespondController@updateCost')->name('responds.setcost');
    Route::post('responds/{id}/accept', 'RespondController@accept')->name('responds.accept');
    Route::get('responds/{id}/reject', 'RespondController@discard')->name('responds.reject');

    Route::post('alert/{notification}', 'NotificationController@read')->name('notifications.read');

    Route::get('logout', 'Auth\LoginController@logout')->name('logout.get');
});

Route::get('/', 'HomeController@index')->name('index');

Route::get('/about', 'StaticController@about')->name('about');
Route::get('/rules', 'StaticController@rules')->name('rules');
Route::get('/faq', 'StaticController@faq')->name('faq');
Route::get('/privacy-policy', 'StaticController@privacyPolicy')->name('privacy-policy');
Route::get('/user-agreement', 'StaticController@userAgreement')->name('user-agreement');

Route::prefix('faq')->group(function () {
    Route::get('pin', 'StaticController@pin')->name('faq.pin');
    Route::get('money-out', 'StaticController@moneyOut')->name('faq.money-out');
    Route::get('account', 'StaticController@account')->name('faq.account');
    Route::get('cost', 'StaticController@cost')->name('faq.cost');
    Route::get('what', 'StaticController@what')->name('faq.what');
    Route::get('freese', 'StaticController@freese')->name('faq.freese');
    Route::get('fee', 'StaticController@fee')->name('faq.fee');
    Route::get('payment', 'StaticController@payment')->name('faq.payment');
});

Route::get('orders', 'OrderController@index')->name('orders');
Route::get('orders/personal', 'OrderController@personal')->name('orders.personal');
Route::get('orders/{order}', 'OrderController@show')->name('orders.show');
Route::get('users/{user}', 'UserController@show')->name('users.show');

Route::post('responds/{respond}/comment', 'RespondController@comment')->name('responds.comment');
Route::post('/users/{id}/safecrow', 'UserController@createSafecrowProfile')->name('users.safecrowprofile');

Auth::routes();
Route::group(['as'=>'oauth.','prefix'=>'oauth'],function(){
  Route::get('/login','Auth\OAuthController@login')->name('login');
  Route::get('/callback','Auth\OAuthController@callback')->name('callback');
  Route::get('/user','Auth\OAuthController@getUser')->name('user');
});


Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/', 'AdminController@index')->name('index');
    Route::get('/orders/{id}/complaintread', 'AdminController@readComplaints')->name('markRead');
    Route::get('/orders/{order}/ban', 'OrderController@ban')->name('orders.ban');
});