<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1404735829629643',
        'client_secret' => '63de9f5bd61f9e8ee476f8e6f5bf4b8c',
        'redirect' => 'http://localhost:8080/oauth/callback'
    ],

    'vkontakte' => [
        'client_id' => '6637687',
        'client_secret' => 'oV7wSwHNE2ASebs4liSF',
        'redirect' => 'http://localhost:8080/oauth/callback'
    ],

    'odnoklassniki' => [
        'client_id' => '1268952832',
        'client_secret' => 'C9EF284871EF5BC534226E49',
        'client_public' => 'CBAQKDKMEBABABABA',
        'redirect' => 'http://localhost:8080/oauth/callback'
    ]

];
