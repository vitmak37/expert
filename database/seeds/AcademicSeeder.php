<?php

use Illuminate\Database\Seeder;

class AcademicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\App\AcademicTitle::get()->count() > 0) {
            print('AcademicTitle table not empty: skiped');
            return;
        }
        \App\AcademicTitle::create(['name' => 'Отсутствует']);
        \App\AcademicTitle::create(['name' => 'Доцент']);
        \App\AcademicTitle::create(['name' => 'Профессор']);

        \App\AcademicDegree::create(['name' => 'Отсутствует']);
        \App\AcademicDegree::create(['name' => 'Кандидат наук']);
        \App\AcademicDegree::create(['name' => 'Доктор наук']);
    }
}
