<?php

use App\WorkType;
use Illuminate\Database\Seeder;

class WorkTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (WorkType::all()->count() > 0) {
            print('Sorry, WorkTypes table is not empty. Skipping');
            return ;
        }

        WorkType::create(['name' => 'Курсовая работа', 'work_amount' => 40, 'cost' => 1000]);
        WorkType::create(['name' => 'Лабораторная работа', 'work_amount' => 30, 'cost' => 1000]);
        WorkType::create(['name' => 'Повышение уникальности', 'work_amount' => 100, 'cost' => 1000]);
        WorkType::create(['name' => 'Перевод', 'work_amount' => 50, 'cost' => 1000]);
        WorkType::create(['name' => 'Копирайтинг', 'work_amount' => 80, 'cost' => 1000]);
        WorkType::create(['name' => 'Монография', 'work_amount' => 180, 'cost' => 1000]);
        WorkType::create(['name' => 'Отчет о практике', 'work_amount' => 120, 'cost' => 1000]);
        WorkType::create(['name' => 'Дипломная работа', 'work_amount' => 80, 'cost' => 3000]);
        WorkType::create(['name' => 'Магистерская диссертация', 'work_amount' => 120, 'cost' => 5000]);
        WorkType::create(['name' => 'Бизнес-план', 'work_amount' => 30, 'cost' => 1500]);
        WorkType::create(['name' => 'Эссе', 'work_amount' => 10, 'cost' => 500]);
        WorkType::create(['name' => 'Реферат', 'work_amount' => 30, 'cost' => 500]);
        WorkType::create(['name' => 'Контрольная работа', 'work_amount' => 30, 'cost' => 500]);
        WorkType::create(['name' => 'Статья', 'work_amount' => 30, 'cost' => 500]);
        WorkType::create(['name' => 'Доклад', 'work_amount' => 30, 'cost' => 500]);
        WorkType::create(['name' => 'Рецензия', 'work_amount' => 30, 'cost' => 500]);
        WorkType::create(['name' => 'Сочинение', 'work_amount' => 30, 'cost' => 500]);
        WorkType::create(['name' => 'Ответы на вопросы', 'work_amount' => 30, 'cost' => 1]);
        WorkType::create(['name' => 'Чертеж', 'work_amount' => 30, 'cost' => 1]);
        WorkType::create(['name' => 'Помощь онлайн', 'work_amount' => 30, 'cost' => 1]);
        WorkType::create(['name' => 'Решение задач', 'work_amount' => 30, 'cost' => 1]);
        WorkType::create(['name' => 'Другое', 'work_amount' => 30, 'cost' => 1]);
    }
}
