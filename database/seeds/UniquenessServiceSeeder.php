<?php

use App\UniqnessService;
use Illuminate\Database\Seeder;

class UniquenessServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (UniqnessService::all()->count() > 0) {
            print("Sorry, UniqnessService table is not empty. Skipping \n\r");
            return;
        }

        UniqnessService::create(['name' => 'Антиплагиат.ру']);
    }
}
