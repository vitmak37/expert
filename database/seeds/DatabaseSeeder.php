<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(WorkTypesSeeder::class);
        $this->call(UniquenessServiceSeeder::class);
        $this->call(AcademicSeeder::class);
        $this->call(KnowledgeAreaSeeder::class);
        $this->call(SpecializationsSeeder::class);
        $this->call(SubjectsSeeder::class);
        $this->call(GuaranteeTermsSeeder::class);
    }
}
