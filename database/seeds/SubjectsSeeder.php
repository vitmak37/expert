<?php

use Illuminate\Database\Seeder;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\App\Subject::get()->count() > 0) {
            print('Subject table not empty: skiped');
            return;
        }
        $gymId = \App\KnowledgeArea::where('name', 'Гуманитарные')->first()->id;
        \App\Subject::create(['name' =>'Актерское мастерство', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Английский язык', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Библиотечно-информационная деятельность', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Дизайн', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Документоведение и архивоведение', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Журналистика', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Искусство', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Китайский язык', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Конфликтология', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Краеведение', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Криминалистика', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Кулинария', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Культурология', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Литература', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Логика', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Международные отношения', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Музыка', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Парикмахерское искусство', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Педагогика', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Политология', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Право и юриспруденция', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Психология', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Реклама и PR', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Религия', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Русский язык', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Связи с общественностью', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Социальная работа', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Социология', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Физическая культура', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Философия', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Этика', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Языки (переводы)', 'knowledge_area_id' => $gymId]);
        \App\Subject::create(['name' =>'Языкознание и филология', 'knowledge_area_id' => $gymId]);

        $techId = \App\KnowledgeArea::where('name', 'Технические')->first()->id;
        \App\Subject::create(['name' =>'Авиационная и ракетно-космическая техника', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Автоматизация технологических процессов', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Автоматика и управление', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Архитектура и строительство', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Базы данных', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Военное дело', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Высшая математика', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Геометрия', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Гидравлика', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Детали машин', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Железнодорожный транспорт', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Инженерные сети и оборудование', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Информатика', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Информационная безопасность', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Информационные технологии', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Материаловедение', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Машиностроение', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Металлургия', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Метрология', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Механика', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Микропроцессорная техника', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Начертательная геометрия', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Приборостроение и оптотехника', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Программирование', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Процессы и аппараты', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Сопротивление материалов', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Сопротивление материалов', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Текстильная промышленность', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Теоретическая механика', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Теория машин и механизмов', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Теплоэнергетика и теплотехника', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Технологические машины и оборудование', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Технология продовольственных продуктов и товаров', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Транспортные средства', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Физика', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Черчение', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Электроника, электротехника, радиотехника', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Энергетическое машиностроение', 'knowledge_area_id' => $techId]);
        \App\Subject::create(['name' =>'Ядерные физика и технологии', 'knowledge_area_id' => $techId]);

        $ecId = \App\KnowledgeArea::where('name', 'Экономические')->first()->id;
        \App\Subject::create(['name' =>'Анализ хозяйственной деятельности', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Антикризисное управление', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Банковское дело', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Бизнес-планирование', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Бухгалтерский учет и аудит', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Внешнеэкономическая деятельность', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Гостиничное дело', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Государственное и муниципальное управление', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Деньги', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Инвестиции', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Инновационный менеджмент', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Кредит', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Логистика', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Маркетинг', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Менеджмент', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Менеджмент организации', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Микро-, макроэкономика', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Налоги', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Организационное развитие', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Производственный маркетинг и менеджмент', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Рынок ценных бумаг', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Стандартизация', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Статистика', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Страхование', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Таможенное дело', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Теория управления', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Товароведение', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Торговое дело', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Туризм', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Управление качеством', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Управление персоналом', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Управление проектами', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Финансовый менеджмент', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Финансы', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Ценообразование и оценка бизнеса', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Эконометрика', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Экономика', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Экономика предприятия', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Экономика труда', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Экономическая теория', 'knowledge_area_id' => $ecId]);
        \App\Subject::create(['name' =>'Экономический анализ', 'knowledge_area_id' => $ecId]);

        $esId = \App\KnowledgeArea::where('name', 'Естественные')->first()->id;
        \App\Subject::create(['name' =>'Астрономия', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Безопасность жизнедеятельности', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Биология', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Ветеринария', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'География', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Геодезия', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Геология', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Естествознание', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Медицина', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Нефтегазовое дело', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Фармация', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Химия', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Хирургия', 'knowledge_area_id' => $esId]);
        \App\Subject::create(['name' =>'Экология', 'knowledge_area_id' => $esId]);
    }
}
