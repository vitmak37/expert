<?php

use Illuminate\Database\Seeder;

class KnowledgeAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\App\KnowledgeArea::get()->count() > 0) {
            print('KnowledgeArea table not empty: skiped');
            return;
        }

        \App\KnowledgeArea::create(['name' => 'Гуманитарные', 'costX' => 1]);
        \App\KnowledgeArea::create(['name' => 'Экономические', 'costX' => 1.1]);
        \App\KnowledgeArea::create(['name' => 'Технические', 'costX' => 1.2]);
        \App\KnowledgeArea::create(['name' => 'Естественные', 'costX' => 1.1]);
    }
}
