<?php

use Illuminate\Database\Seeder;

class GuaranteeTermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\App\GuaranteeTerm::get()->count() > 0) {
            print('KnowledgeArea table not empty: skiped');
            return;
        }

        \App\GuaranteeTerm::create([
            'name' => 'Стандартная гарантия',
            'guarantee_terms' => 'Гарантия на 30 дней после сдачи работы',
            'days' => 30
        ]);
    }
}
