<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusWaitingForAcceptToOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::statement("ALTER TABLE `orders` MODIFY `status` enum('draft', 'accepting', 'auction', 'underway', 'pending', 'declined', 'argue', 'done', 'guarantee', 'banned', 'payment') NOT NULL DEFAULT 'draft';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
