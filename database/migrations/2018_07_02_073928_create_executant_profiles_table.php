<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecutantProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executant_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('qualification')->nullable();
            $table->text('about')->nullable();
            $table->string('education')->nullable();
            $table->enum('status', ['active'])->nullable();
            $table->unsignedInteger('academic_title_id')->nullable();
            $table->unsignedInteger('academic_degree_id')->nullable();
            $table->unsignedInteger('files_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('executant_profiles');
    }
}
