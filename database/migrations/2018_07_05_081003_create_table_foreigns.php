<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForeigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('roles_id')->references('id')->on('site_roles')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('executant_profile_id')->references('id')->on('executant_profiles')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('customer_profile_id')->references('id')->on('customer_profiles')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('avatar_file_id')->references('id')->on('files')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('social_users', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('site_notifications', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('ratings', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('executant_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('executant_profiles', function (Blueprint $table) {
            $table->foreign('academic_degree_id')->references('id')->on('academic_degrees')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('academic_title_id')->references('id')->on('academic_titles')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('files_id')->references('id')->on('files')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('executant_profile_has_work_types', function (Blueprint $table) {
            $table->foreign('executant_profile_id',
                'ep_wt_foreign')->references('id')->on('executant_profiles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('work_types_id',
                'wt_ep_foreign')->references('id')->on('work_types')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('executant_profile_has_specialization', function (Blueprint $table) {
            $table->foreign('executant_profile_id',
                'ep_sp_foreign')->references('id')->on('executant_profiles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('specialization_id',
                'sp_ep_foreign')->references('id')->on('specializations')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('work_types_orders', function (Blueprint $table) {
            $table->foreign('orders_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('work_types_id')->references('id')->on('work_types')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('subject_orders', function (Blueprint $table) {
            $table->foreign('orders_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('subjects_id')->references('id')->on('subjects')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('winned_respond')->references('id')->on('responds')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('uniqueness_service_id')->references('id')->on('uniqness_services')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('guarantee_terms_id')->references('id')->on('guarantee_terms')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('complaints', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('site_comments', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('responds_id')->references('id')->on('responds')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('responds', function (Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('orders_has_files', function (Blueprint $table) {
            $table->foreign('orders_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('files_id')->references('id')->on('files')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('comments_has_file', function (Blueprint $table) {
            $table->foreign('comment_id')->references('id')->on('site_comments')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('file_id')->references('id')->on('files')->onUpdate('cascade')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_roles_id_foreign');
            $table->dropForeign('users_executant_profile_id_foreign');
            $table->dropForeign('users_customer_profile_id_foreign');
            $table->dropForeign('users_avatar_file_id_foreign');
        });

        Schema::table('social_users', function (Blueprint $table) {
            $table->dropForeign('social_users_user_id_foreign');
        });

        Schema::table('site_notifications', function (Blueprint $table) {
            $table->dropForeign('site_notifications_user_id_foreign');
        });

        Schema::table('ratings', function (Blueprint $table) {
            $table->dropForeign('ratings_user_id_foreign');
            $table->dropForeign('ratings_executant_id_foreign');
        });

        Schema::table('executant_profiles', function (Blueprint $table) {
            $table->dropForeign('executant_profiles_academic_degree_id_foreign');
            $table->dropForeign('executant_profiles_academic_title_id_foreign');
            $table->dropForeign('executant_profiles_files_id_foreign');
        });

        Schema::table('executant_profile_has_work_types', function (Blueprint $table) {
            $table->dropForeign('ep_wt_foreign');
            $table->dropForeign('wt_ep_foreign');
        });

        Schema::table('executant_profile_has_specialization', function (Blueprint $table) {
            $table->dropForeign('ep_sp_foreign');
            $table->dropForeign('sp_ep_foreign');
        });

        Schema::table('work_types_orders', function (Blueprint $table) {
            $table->dropForeign('work_types_orders_orders_id_foreign');
            $table->dropForeign('work_types_orders_work_types_id_foreign');
        });

        Schema::table('subject_orders', function (Blueprint $table) {
            $table->dropForeign('subject_orders_orders_id_foreign');
            $table->dropForeign('subject_orders_subjects_id_foreign');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_user_id_foreign');
            $table->dropForeign('orders_uniqueness_service_id_foreign');
            $table->dropForeign('orders_guarantee_terms_id_foreign');
        });

        Schema::table('complaints', function (Blueprint $table) {
            $table->dropForeign('complaints_user_id_foreign');
            $table->dropForeign('complaints_order_id_foreign');
        });

        Schema::table('site_comments', function (Blueprint $table) {
            $table->dropForeign('site_comments_user_id_foreign');
            $table->dropForeign('site_comments_responds_id_foreign');
        });

        Schema::table('responds', function (Blueprint $table) {
            $table->dropForeign('responds_order_id_foreign');
            $table->dropForeign('responds_user_id_foreign');
        });

        Schema::table('orders_has_files', function (Blueprint $table) {
            $table->dropForeign('orders_has_files_orders_id_foreign');
            $table->dropForeign('orders_has_files_files_id_foreign');
        });

        Schema::table('comments_has_file', function (Blueprint $table) {
            $table->dropForeign('comments_has_file_comment_id_foreign');
            $table->dropForeign('comments_has_file_file_id_foreign');
        });
    }
}
