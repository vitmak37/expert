<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->enum('status', ['draft', 'auction', 'payment', 'underway', 'pending', 'declined', 'argue', 'done', 'guarantee', 'banned'])->default('draft');
            $table->string('title');
            $table->text('description');
            $table->string('work_amount');
            $table->dateTime('deadline');
            $table->unsignedInteger('uniqueness_service_id');
            $table->integer('uniqueness_percentage');
            $table->integer('budget');
            $table->unsignedInteger('guarantee_terms_id');
            $table->string('ban_reason')->nullable();
            $table->unsignedInteger('winned_respond')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
