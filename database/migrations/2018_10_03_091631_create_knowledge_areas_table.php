<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKnowledgeAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowledge_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('costX', 3,2);
            $table->timestamps();
        });

        Schema::table('subjects', function (Blueprint $table) {
            $table->unsignedInteger('knowledge_area_id');
            $table->foreign('knowledge_area_id')->references('id')->on('knowledge_areas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knowledge_areas');
    }
}
