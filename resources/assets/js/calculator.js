import Calculator from './components/Calculator';
Vue.component('calculator', Calculator);

import SocAuth from './components/SocialAuth';
Vue.component('social-auth', SocAuth);

import Vuex from 'vuex';

const store = new Vuex.Store({
    state: {
        auth: {
            form: 'login',
            show: false,
            user: {}
        }
    },
    mutations: {
        auth_go_login_form(state) {
            state.auth.form = 'login';
        },
        auth_go_register_form(state) {
            state.auth.form = 'register';
        },
        auth_toggle_modal(state) {
            state.auth.show = !state.auth.show;
        },
        auth_set_user(state, data) {
            state.auth.user = data;
        }
    },
    getters: {
        auth_get_user: state => state.auth.user,
    }
});

if (document.querySelector('#vue')) {
    var vuejs = new Vue({
        el: '#vue',
        store
    });
}

if (document.querySelector('#auth-vue')) {
    var authVue = new Vue({
        el: '#auth-vue',
        store
    });
}

if (document.querySelector('#register-vue')) {
    var registerVue = new Vue({
        el: '#register-vue',
        store
    });
}