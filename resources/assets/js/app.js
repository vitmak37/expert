require('./bootstrap');
require('./dependencies/selectize/selectize.min.js');
require('./dependencies/selectize/selectize.bootstrap3.css');
require('./dependencies/datepicker.min');
require('./calculator');
window.moment = require('moment');

var basePath = '';

var loginPath = basePath + '/login';
var registerPath = basePath + '/register';

$('.js-selectize').selectize();

var elements = document.querySelectorAll('.js-hidden-open');
if(elements){
    elements.forEach((el, i, arr) => {
        el.addEventListener('click', (e) => {
            el.parentElement.classList.toggle('hidden__info_open');
        });
    })
}


$('#respondModal').on('show.bs.modal', function (event) {
   let btn = $(event.relatedTarget);
   var orderid = btn.data('orderid');
   var modal = $(this);
   console.log(orderid);
   modal.find('#orderId').val(orderid);
   modal.find('#orderIdName').text(orderid);
});

$('#complaintModal').on('show.bs.modal', function(event){
   let btn = $(event.relatedTarget);
   var orderid = btn.data('orderid');
   var modal = $(this);
   modal.find('#complaintModalOrderid').val(orderid);
});

$('.js-change-password')


function login()
{
    let loginPassword = document.querySelector('#auth_password');
    let loginLogin = document.querySelector('#auth_login');
    let loginRemember = document.querySelector('#auth_remember');

    document.querySelector('#auth_login').classList.remove('is-invalid');
    document.querySelector('#auth_password').classList.remove('is_invalid');
    document.querySelector('#auth_login_invaliddescription').textContent = '';
    document.querySelector('#auth_login_invalidpwdescription').textContent = '';

    axios.post(loginPath, {
        email: loginLogin.value,
        password: loginPassword.value,
        loginRemember: loginRemember == null,
        "_token": document.head.querySelector('meta[name="csrf-token"]')
    }).then(response => {
        if (!! response.data.redirect)
            window.location.href = response.data.redirect;
    }).catch(error => {
        if(!!error.response.data.errors){
            if(!!error.response.data.errors.email) {
                document.querySelector('#auth_login').classList.add('is-invalid');
                document.querySelector('#auth_login_invaliddescription').textContent = error.response.data.errors.email;
            }

            if(!!error.response.data.password) {
                document.querySelector('#auth_password').classList.add('is-invalid');
                document.querySelector('#auth_login_invaliddescription').textContent = error.response.data.errors.password;
            }
        }
    });
}

if(document.querySelector('#registerSubmit') && document.querySelector('#loginSubmit')){
    document.querySelector('#registerSubmit').addEventListener('click', register);
    $('#changeLogin').on('click', changeModalLogin);
    $('#changeRegister').on('click', changeModalRegister);
    document.querySelector('#loginSubmit').addEventListener('click', login);
}
function register()
{
    let registerEmail = document.querySelector('#email');
    let registerLogin = document.querySelector('#login');
    let registerPassword = document.querySelector('#password');
    let registerPasswordConfirmation = document.querySelector('#password_confirm');
    let terms = document.querySelector('#terms');

    document.querySelector('#email').classList.remove('is-invalid');
    document.querySelector('#password').classList.remove('is-invalid');
    document.querySelector('#password_confirm').classList.remove('is-invalid');
    document.querySelector('#login').classList.remove('is-invalid');

    axios.post(registerPath, {
        email: registerEmail.value,
        password: registerPassword.value,
        password_confirmation: registerPasswordConfirmation.value,
        username: registerLogin.value,
        terms: !!terms
    }).then(response => {
        if (!! response.data.redirect)
            window.location.href = response.data.redirect;
    }).catch(error => {
        if(!!error.response.data.errors){
            let errors = error.response.data.errors;
            if (!!errors.email) {
                document.querySelector('#email').classList.add('is-invalid');
                document.querySelector('#email_invaliddescription').textContent = error.response.data.errors.email;
            }
            if (!!errors.password) {
                document.querySelector('#password').classList.add('is-invalid');
                document.querySelector('#password_invaliddescription').textContent = error.response.data.errors.password;
            }
            if (!!errors.password_confirmation) {
                document.querySelector('#password_confirm').classList.add('is-invalid');
                document.querySelector('#password_confirmation_invaliddescription').textContent = error.response.data.errors.password_confirmation;
            }
            if (!!errors.username) {
                document.querySelector('#login').classList.add('is-invalid');
                document.querySelector('#login_invaliddescription').textContent = error.response.data.errors.username;
            }
        }
    });
}


function changeModalLogin(e)
{
    e.preventDefault();
    $('#authorizationModal').modal('hide');
    $('#registerModal').modal('show');
}


function changeModalRegister(e)
{
    e.preventDefault();
    $('#registerModal').modal('hide');
    $('#authorizationModal').modal('show');
}

$('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    weekStart: 1,
    startView: 0,
    yearFirst: false,
    yearSuffix: ''
});

$(document).ready(function () {
    $('#successModal').modal('show');
    $('#errorModal').modal('show');
    $('#safecrowUserModal').modal('show');
});