<div class="page__title-container">
    <h2 class="page-title">{{ $title }}</h2>
    <div class="breadcrumps">
        <ul class="breadcrump__list">
            @foreach($breadcrumbs as $breadcrumb)
                <li class="breadcrump__item">
                    <a href="{{ $breadcrumb['link']}}" class="breadcrump__link">{{ $breadcrumb['name'] }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>