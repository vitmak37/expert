<header class="header border-bottom bg-white p-5">
    <div class="container">
        <div class="d-flex align-items-center justify-content-between">
            <div class="m-0 p-0">
                <a href="{{ route('index') }}">
                    <img src="{{ asset('img/logo.svg') }}" alt="" class="logo logo_header">
                </a>
            </div>
            <div class="d-none d-xl-block m-0 p-0">
                @include('layouts.parts.menu')
            </div>
            <div class="d-flex justify-content-end m-0 p-0">
                @if(!auth()->check())
                    <div class="login">
                        <a href="#" class="btn btn-outline-primary" data-target="#authorizationModal"
                           data-toggle="modal">Войти</a>
                    </div>
                @else
                    <div class="dropdown">
                        <a href="#" class="nav-link icon h-100 pr-5" data-toggle="dropdown">
                            <div>
                                <i class="fa fa-bell-o"></i>
                            </div>

                            @if(auth()->user()->anyUnreadNotification())
                                <span class="nav-unread"></span>
                            @endif
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                            @foreach(auth()->user()->unreadNotifications as $notification)
                                <div class="dropdown-item d-flex">
                                    @if($notification->link )
                                        <a href="{{ $notification->link }}">
                                            {!! $notification->text !!}
                                        </a>
                                    @else
                                        <div>
                                            {!! $notification->text !!}
                                        </div>
                                    @endif
                                    <small>
                                        <form class="d-flex align-items-center"
                                              action="{{ route('notifications.read', ['id' => $notification->id])  }}"
                                              method="POST">
                                            @csrf
                                            <button class="btn btn-sm text-success btn-none"><i class="fa fa-check"></i>
                                            </button>
                                        </form>
                                    </small>
                                </div>
                            @endforeach
                            @if (!auth()->user()->anyUnreadNotification())
                                <div class="dropdown-item d-flex">
                                    <span class="text-muted">У вас нет непрочитанных уведомлений</span>
                                </div>
                            @endif
                            {{--<div class="dropdown-divider"></div>--}}
                            {{--<a href="#" class="dropdown-item text-muted-dark text-center">Отметить все прочитанными</a>--}}
                        </div>
                    </div>
                    <div class="d-flex dropdown">
                        <a href="#" class="nav-link p-0 d-flex align-items-center flex-row" data-toggle="dropdown">
                            <span class="avatar mr-md-2 mr-none d-block"
                                  style="background-image: url('{{ auth()->user()->avatar_url  }}')"></span>
                            <div class="d-flex flex-column ml-1">
                                <span class="text-default leading-none font-weight-bold d-md-block d-none mb-1">{{ auth()->user()->username }}</span>
                                <span class="text-muted leading-none d-md-block d-none ">
                                    @if(auth()->user()->type == \App\User::TYPE_CUSTOMER)
                                        Заказчик
                                    @elseif(auth()->user()->type == \App\User::TYPE_EXECUTANT)
                                        Исполнитель
                                    @else
                                        <span class="text-danger">Создайте профиль</span>
                                    @endif
                                </span>
                            </div>
                            <i class="fa fa-chevron-down text-muted ml-0 ml-md-3"></i>
                        </a>
                        @if(auth()->check() && auth()->user()->type)
                            @if(auth()->user()->type == \App\User::TYPE_CUSTOMER)
                                <a href="{{ route('orders.create') }}"
                                   class="btn btn-outline-primary ml-5 d-lg-block d-none">Создать заказ</a>
                            @else
                                <a href="{{ route('orders') }}" class="btn btn-outline-primary ml-5 d-lg-block d-none">Искать
                                    заказ</a>
                            @endif
                        @endif
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ route('profile') }}" class="dropdown-item"><i
                                        class="fa fa-user dropdown-icon"></i> Профиль</a>
                            @if(auth()->check())
                                @can('admin')
                                    <a href="{{ route('admin.index') }}" class="dropdown-item">
                                        <i class="fa fa-pie-chart dropdown-icon"></i> Админка</a>
                                @endcan
                            @endif
                            @if(auth()->check() && auth()->user()->type)
                                <a href="{{ route('orders.personal') }}" class="dropdown-item"><i
                                            class="fa fa-tasks dropdown-icon"></i> Мои заказы</a>
                                @if(!auth()->user()->type == \App\User::TYPE_CUSTOMER || auth()->user()->can('admin'))
                                    <a href="{{ route('orders') }}" class="dropdown-item"><i
                                                class="fa fa-balance-scale dropdown-icon"></i> Биржа заказов</a>
                                @endif
                            @endif
                            <a href="{{ route('logout.get') }}" class="dropdown-item"><i
                                        class="fa fa-sign-out dropdown-icon"></i> Выход</a>
                        </div>
                    </div>
                @endif

                <a href="#" class="header-toggler d-xl-none ml-3 ml-xl-0" data-toggle="collapse"
                   data-target="#collapsableHeader">
                    <span class="header-toggler-icon"></span>
                </a>
            </div>
        </div>
    </div>
</header>

<!-- Togglable Navigation -->
@include('layouts.parts.togglableNav')
<!-- End of togglable navigation -->