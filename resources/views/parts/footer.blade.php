<footer class="page__footer">
    <div class="container">
        <div class="row justify-content-between mr-0 ml-0">
            <div class="footer__description-block">
                <h4 class="footer__title">Эксперт<span class="footer__title_blue">24</span></h4>
                <p class="footer__description">Проект Эксперт24 ‒ это интернет-сервис для помощи и консультации по любым видам учебных работ. Сотни квалифицированных специалистов практически по всем предметам готовы уже сейчас приступить к Вашему заказ</p>
            </div>
            <nav class="footer__nav">
                <ul class="footer__list">
                    <li class="footer__list-item"><a href="{{ route('about') }}" class="footer__link">О проекте</a></li>
                    <li class="footer__list-item"><a href="{{ route('rules') }}" class="footer__link">Правила работы</a></li>
                    <li class="footer__list-item"><a href="{{ route('faq') }}" class="footer__link">Часто задаваемые вопросы</a></li>
                    <li class="footer__list-item"><a href="{{ route('user-agreement') }}" class="footer__link">Пользовательское соглашение</a></li>
                    <li class="footer__list-item"><a href="{{ route('privacy-policy') }}" class="footer__link">Политика конфиденциальности</a></li>
                </ul>
            </nav>
            <div class="footer__social">
                <h5 class="socials__title">Мы в социальных сетях</h5>
                <div class="d-flex">
                    <a href="https://vk.com/public172422913" class="social__link social__link_vk"><i class="fa fa-vk"></i></a>
                    <a href="https://www.facebook.com/groups/2168209820117095/" class="social__link social__link_fb"><i class="fa fa-facebook"></i></a>
                    <a href="https://ok.ru/group/54006645981320" class="social__link social__link_ig"><img src="{{ asset('img/ok.svg') }}" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<footer class="page__last-footer background-white">
    <div class="container">
        <div class="row justify-content-between align-items-center mr-0 ml-0">
            <div class="footer__accept d-none d-sm-flex">
                <div class="accept__item"><img src="{{asset('img/visa.svg')}}" alt=""></div>
                <div class="accept__item"><img src="{{asset('img/mastercard.svg')}}" alt=""></div>
            </div>
            <div class="footer__copyright">"Эксперт24" - &copy; Все права защищены</div>
            
        </div>
    </div>
    
</footer>