<section class="section section__promo">
    <div class="section-promo__container container d-flex align-items-center">
        <div class="row align-items-center justify-content-between">
            <div class="section-promo__image-container col-lg-6 order-1 order-lg-0">
                <img src="{{asset('img/illustration banner.svg')}}" alt="" class="promo__image">
            </div>
            <div class="section-promo__description col-lg-5 order-0 order-lg-1">
                <p class="promo__sign">Эксперт 24</p>
                <h1 class="promo__title">Интернет-биржа студенческих работ</h1>
                <p class="promo__description">Связующие звено между заказчиком и исполнителем учебных заданий. На нашем сервисе вы можете заказать выполнение работы любой сложности и реализовать себя как автор и специалист в своей научной области</p>
                <a href="#" class="btn btn-primary" data-target="#registerModal" data-toggle="modal">Начните работу</a>
            </div>
        </div>
    </div>
</section>