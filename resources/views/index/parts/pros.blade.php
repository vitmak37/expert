<section class="section section__pros">
    <div class="pros-section__title-container">
        <h2 class="pros-section__title">Как мы работаем</h2>
        <p class="pros-section__subtitle">Работать с Эксперт24 просто и удобно. Вы общаетесь напрямую с автором и можете контролировать и корректировать процесс написания работы
            в режиме реального времени. Гарантийный срок проверки работ на правильность и качество исполнения — от 20 дней. Только когда заказчик примет работу или по истечению гарантийного срока, исполнитель получает вознаграждение.</p>
    </div>
    <div class="container">
        <div class="row pros__row">
            <div class="col-md-3">
                <div class="pros__card">
                    <div class="pros__icon">
                        <img src="{{asset('img/1.svg')}}" alt="Создание заказа">
                    </div>
                    <div class="pros__description">
                        <h6 class="pros__title">Создание заказа</h6>
                        <p class="pros__description-text">Укажите параметры работы и сроки сдачи</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                 <div class="pros__card">
                    <div class="pros__icon">
                        <img src="{{asset('img/2.svg')}}" alt="Создание заказа">
                    </div>
                    <div class="pros__description">
                        <h6 class="pros__title">Выберите исполнителя</h6>
                        <p class="pros__description-text">Выберите любого понравившегося автора</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pros__card">
                    <div class="pros__icon">
                        <img src="{{asset('img/4.svg')}}" alt="Создание заказа">
                    </div>
                    <div class="pros__description">
                        <h6 class="pros__title">Оплата заказа</h6>
                        <p class="pros__description-text">Оплатите работу любым удобным для вас способом</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pros__card">
                    <div class="pros__icon">
                        <img src="{{asset('img/3.svg')}}" alt="Создание заказа">
                    </div>
                    <div class="pros__description">
                        <h6 class="pros__title">Готовая работа</h6>
                        <p class="pros__description-text">После оплаты заказа получите готовую работу</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>