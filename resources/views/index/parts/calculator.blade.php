<div id="vue">
    <calculator
        :_work_types="{{ json_encode(\App\WorkType::all()) }}"
        :_subjects="{{ json_encode(\App\Subject::all()) }}"
    ></calculator>
</div>