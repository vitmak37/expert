@section('title', 'Эксперт24 - Биржа студенческих работ')

@extends('layouts.base')

@section('body')
    @include('parts.header')
    <div class="content">
        @section('content')
            @show
    </div>
    @include('parts.footer')
    @if(!auth()->check())
        @include('modals.register')
        @include('modals.authorization')
    @else
        @if(session()->has('successModal'))
            @include('modals.success', ['successTitle' => session('successModal')['title'], 'successMessage' => session('successModal')['message']])
        @endif

        @if(session()->has('errorModal'))
            @include('modals.error', ['errorTitle' => session('errorModal')['title'], 'errorMessage' => session('errorModal')['message']])
        @endif

        @if(auth()->user()->hasSafecrowAccount() && !auth()->user()->hasAssignedCard())
            @include('modals.safecrowcardassign')
        @endif
    @endif
@endsection