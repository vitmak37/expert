<ul class="nav d-flex justify-content-center">
    <li class="nav-item">
        <a href="{{ route('about') }}" class="nav-link nav-link_header p-0">О проекте</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('rules') }}" class="nav-link nav-link_header p-0">Правила работы</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('faq') }}" class="nav-link nav-link_header p-0">Часто задаваемые вопросы</a>
    </li>
</ul>