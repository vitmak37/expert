<meta name="description" content="Эксперт24 - платформа для заказа студенческих работ онлайн непосредственно у исполнителей. Гарантия результата и качества выполненных работ">
<meta name="keywords" content="студенческие работы, маркетплейс, курсовая работа, дипломная работа, лабораторная работа, бизнес-план, заказ работ, контрольная работа">
<meta name="robots" content="index,follow">
<meta property="og:title" content=" биржа студенческих работ" />
<meta property="og:type" content="website" />
<meta property="og:url" content=" " />
<meta property="og:image" content="/img/logo.svg" />
<meta property="og:description" content="Эксперт24 - платформа для заказа студенческих работ онлайн непосредственно у исполнителей. Гарантия результата и качества выполненных работ" />