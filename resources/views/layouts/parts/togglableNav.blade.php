<header class="header p-5 p-md-3 pl-0 pr-0 collapse d-xl-none" id="collapsableHeader">
    <div class="container">
        <div class="row">
            <ul class="nav d-flex flex-column flex-md-row flex-wrap col-12 col-md-9">
                <li class="nav-item pt-3 pb-3 pl-0 pr-0">
                    <a href="{{ route('about') }}" class="nav-link nav-link_header">О проекте</a>
                </li>
                <li class="nav-item pt-3 pb-3 pl-0 pr-0">
                    <a href="{{ route('rules') }}" class="nav-link nav-link_header">Правила работы</a>
                </li>
                <li class="nav-item pt-3 pb-3 pl-0 pr-0">
                    <a href="{{ route('faq') }}" class="nav-link nav-link_header">Часто задаваемые вопросы</a>
                </li>
            </ul>
            <div class="col-12 col-md-3 d-flex justify-content-start align-items-start justify-content-md-end mt-3 mt-sm-0">
                @if(auth()->check() && auth()->user()->type)
                    @if(auth()->user()->type == \App\User::TYPE_CUSTOMER)
                        <a href="{{ route('orders.create') }}" class="btn btn-outline-primary ml-5 d-lg-block d-none">Создать
                            заказ</a>
                    @else
                        <a href="{{ route('orders') }}" class="btn btn-outline-primary ml-5 d-lg-block d-none">Искать
                            заказ</a>
                    @endif
                @endif
            </div>

        </div>

    </div>
</header>