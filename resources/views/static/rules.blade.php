@extends('layouts.default')

@section('content')
    @include('parts.breadcrumbs', ['title' => 'О проекте', 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('rules'), 'name' => 'Правила работы']
        )
    ])
    <section>
        <div class="container pb-9">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body font-size-1">
                            <h3>Правила работы</h3>
                            <h4>Для Заказчика</h4>
                            <p><b>Шаг 1. Размещение работы.</b>  Максимально подробное описание задачи — это поможет Исполнителям точнее оценить её сложность и определить правильную стоимость. Прикрепление вспомогательных файлов.</p>
                            <p><b>Шаг 2. Работа переходит в аукцион.</b> После выставления Заказа, Исполнители начнут предлагать свои услуги. Вы сможете выбрать подходящего Вам Исполнителя.</p>
                            <p><b>Шаг 3. Выбор Исполнителя.</b> Выбрав Исполнителя, необходимо дождаться от него подтверждения, что он готов взять Работу и выполнить её в срок. На Вашу почту поступит письмо, что Исполнитель готов приступить к работе.</p>
                            <p><b>Шаг 4. Оплата.</b> Оплата стоимости Работы (после выбора Исполнителя), производится любым удобным и доступным способом. В случаи, если Работа Вас не удовлетворит, мы вернём Вам деньги.</p>
                            <p><b>Шаг 5. Общение.</b> Общение с Исполнителем в процессе работы происходит на прямую, без посредников, в том числе получение промежуточных вариантов Работы.</p>
                            <p><b>Шаг 6. Гарантия.</b> Исполнитель получит вознаграждение за выполненную работу, только при окончании гарантийного срока. Корректировки в Заказ можно вносить бесплатно, в течении 20 дней, после завершения Работы.</p>
                            <p><b>Шаг 7. Оценка Работы Автора.</b> Мы будем благодарны, если после выполнения Заказа, Вы оставите отзыв о Исполнителе. Это поможет другим Пользователям определяться с выбором Исполнителя.</p>
                            <p><b>Шаг 8. История заказов.</b> В разделе «Мои заказы», Вы сможете в любое время просмотреть все текущие и выполненные заказы.</p>
                            <br>
                            <h4>Для Исполнителя</h4>
                            <p><b>Шаг 1. Выбор подходящего задания.</b> В разделе «Аукцион заказов», размещаются тысяча заказов ежедневно. С помощью фильтра, легко найти заказ, отвечающий Вашей специализации.</p>
                            <p><b>Шаг 2. Ознакомление с описанием работы.</b> К каждому Заказу имеется подробное описание требований Заказчика.</p>
                            <p><b>Шаг 3. Оценка стоимости.</b> После выбора Заказа, предложите свои услуги и укажите стоимость. Пишите комментарии к ставкам, это даст возможность Заказчику убедится в Вашей компетентности в данном вопросе.</p>
                            <p><b>Шаг 4. Подтверждение на выполнение работы.</b> После выбора Заказчиком Вас, как Исполнителя, следует подтвердить свою готовность выполнить в срок Заказ в течении 12 часов. После этого можете приступать к работе.</p>
                            <p><b>Шаг 5. Общение с Заказчиком.</b> Общение с Заказчиком в процессе работы осуществляются на прямую, в том числе получение уточняющей информации от Заказчика.</p>
                            <p><b>Шаг 6. Загрузка окончательного варианта.</b> После выполнения задания, следует загрузить его окончательный вариант.</p>
                            <p><b>Шаг 7. Получение вознаграждение.</b> После выполнения Заказа, наступает гарантийный срок, по завершении которого Вы получите денежные средства на счёт. Гарантийный срок, при досрочном принятии Работы, может быть отменён.</p>
                            <p><b>Шаг 8. Любите своих Заказчиков.</b> Ваш рейтинг в системе основывается на оценке Ваших Работ Заказчиком. Чем выше рейтинг, тем больше Заказчиков будут отдавать Вам предпочтение, как Исполнителю.</p>
                            <br>
                            <h4>Правила работы</h4>
                            <p>Работа в системе проста и удобна. От Вас требуется — соблюдение основных правил:</p>
                            <p>Заполняя анкету, Исполнитель должен указывать достоверную информацию, если контактные данные окажутся не верные, то аккаунт будет заблокирован.</p>
                            <p>Общение между Заказчиком и Исполнителем допустимо лишь на страницах Платформы. Запрещено обмениваться контактными данными (e-mail, id, skype, телефон и т. д.). В случаи выявления таких моментов, Компания заблокирует аккаунт Пользователя.</p>
                            <p>При выполнении Заказа, Исполнитель обязан соблюдать сроки и предоставлять Работу в соответствии с требованиями Заказчика. Спустя месяц, по завершению Заказа, возможность писать комментарии или загружать файлы и для Заказчика, и для Исполнителя — закрывается.</p>
                            <p>В случаи если после сдачи Работы Заказчик или отдел качества сервиса Эксперт24 в течении 20 дней выставит обоснованное замечание или требование к её качеству, Исполнитель обязан исправить недостатки в 3-х дневный срок безвозмездно.</p>
                            <p>Платформа, имеет право в одностороннем порядке расторгнуть сотрудничество в случаях, если: сам Исполнитель хочет расторгнуть сотрудничество; Исполнитель пренебрегает требованиями Заказчика или администраторов системы; не выполняет корректировки Работы; срывает сроки выполнения; допускает плагиат; не выходит на связь; не выполняет в течении 6 месяцев ни одной работы; регистрируется на Платформе повторно, после блокировки первого аккаунта; осуществляется работа с 2-х и более аккаунтов одновременно.</p>
                            <p>Заказчик или Платформа может размещать сведения о Исполнителе в различных чёрных списках, если он приносит сервису Эксперт24 или Заказчику финансовый или репутационный урон. Это бывает в случаях, когда Исполнитель периодически нарушает данные правила, или требования Заказчика или Платформы (сорвал сроки более чем на 3 дня; представил Работу содержащею плагиат в больших объёмах).</p>
                            <p>Если Исполнитель периодически срывает сроки, систематически нарушает правила или выполняет Работы не качественно, то Платформа имеет право блокировать аккаунт Исполнителя или обнулить его рейтинг.</p>
                            <h5>Исполнителю запрещено:</h5>
                            <p>Использовать Платформу в качестве источника, заменяющего литературу.</p>
                            <p>Использовать сайт готовых рефератов, курсовых и дипломных работ.</p>
                            <p>Исполнитель несёт полную материальную ответственность согласно законодательства своей страны, и подтверждает, что результат Работы не нарушает прав третьих лиц.</p>
                            <p>Заявки на выплату денежных средств из личного кабинета Платформа обрабатывает в ручном режиме в буднее дни, в рабочее время. Поданная заявка получает одобрение на 3 день после её подачи. Зачисление одобренных средств происходят в течении нескольких минут на Яндекс.Деньги и Вебмани, на Киви-кошелек и мобильный телефон — в течении 2-х дней, на банковскую карту — от нескольких минут до 5-ти рабочих дней. При этом, дата подачи заявки не учитывается.</p>
                            <p>Оплата вознаграждения Исполнителю является подтверждением передачи авторских прав на Работу от Исполнителя Заказчику. После этого, Исполнителю запрещено использовать свою Работу в любых целях на территории РФ и за её приделами. Окончательный результат Работы Исполнителя должен предоставить в виде, который соответствует всем правилам оформления и требованиям Заказчика. Загрузка выполненной Работы в систему производится в электронном виде.</p>
                            <p>При предъявлении требований Заказчика на возврат денежных средств за некачественно выполненную Работу, Эксперт24 обязуется ознакомить Исполнителя с данными требованиями. Если урегулирование конфликтной ситуации путём  достижение взаимных договорённостей не возможно, Платформа имеет право на привлечение независимых экспертов для оценки качества Работы Исполнителя. Срок, отведённый на рассмотрение спорных ситуаций — 5 рабочих дней.</p>
                            <p>Полный (100%) возврат денежных средств Заказчику производится, если:</p>
                            <ul>
                                <li>файл с Работой Исполнителя не был загружен;</li>
                                <li>в тексте использованы технические средства обмана (автоплагиат);</li>
                                <li>выполненная Работа не соответствует заданной теме.</li>
                            </ul>
                            <p>
                                В требовании на возврат или перерасчёт средств, Заказчик должен указать причины и привести примеры недостатков Работы.</p>
                            <p>Частичный (до 90%) возврат денег Заказчику производится в случаи, если Исполнитель предоставил Работу, но не сделал требуемые корректировки (Заказчик должен указать, какой % корректировок не выполнен). Компания имеет право отказать Заказчику в проведении перерасчёта, если Заказчик не отправлял Работу на корректировку, но подал заявку на перерасчёт. Если Исполнитель находится на Платформе и готов доработать Заказ, то Заказчик должен отправить Работу на корректировку. В этом случаи, не зависимо от времени окончания гарантийного срока, Платформа может вернуть выполненный Заказ в гарантийный период и осуществить повторную проверку Исполнителя на соблюдение правил работы на Платформы. Если у Заказчика будут претензии к качеству Работы Исполнителя, то выплаты приостанавливаются, до момента разрешения спорных ситуаций.
                            </p>
                            <p>Эксперт24 может применять штрафные санкции к Исполнителю, в случаи нарушения им сроков и при несоответствии Работы требованиям Заказчика. На середине срока сдачи Заказа, Исполнитель обязан подтвердить выполнение Работы в установленный срок, в противном случаи Заказ будет выставлен в аукцион, а рейтинг Исполнителя снижен.</p>
                            <p>Оплата Заказа производится только через сайт. Запрещено осуществлять оплату любыми другими способа вне сайта: онлайн-кошельки, банковские карты, мобильные телефоны и т. д. В случаи выявления попыток обмена  платежами вне сайта, Компания блокирует аккаунт и снимает все взятые на себя гарантийные обязательства по Заказу.</p>
                            <p>В спорных ситуациях между Заказчиком и Исполнителем, в рамках гарантийного срока Заказа, результаты экспертизы, проведённые экспертным отделом Платформы, признаются окончательными и пересмотру не подлежат. Результат экспертизы — внутренний документ для пользования сотрудников Платформы и не предоставляется Пользователям для ознакомления.</p>
                            <p>Данное правило обязательно для ознакомления Исполнителям, оказывающими услуги на Платформе. Работа на Платформе — полное и безусловное подтверждение согласия с данными Правилами.</p>
                            <p>Основные параметры типовой работы, являющиеся обязательными для выполнения Исполнителям:</p>
                            <ul>
                                <li>Исполнитель обязан выполнить Работу в полном соответствии с требованиями Заказчика: структура и тематика, количество используемых в работе источников, количество страниц Работы, тип шрифта и межстрочный интервал. При отсутствии данных параметром, используются стандартные: 14 шрифт Times New Roman, 1,5 интервал.</li>
                                <li>Оформление — необходимо наличие подзаголовков и подразделов Работы, и указание номеров страниц. При чём, заголовки должны быть ссылками на соответствующие страницы. Ознакомиться с методикой создания раздела «Содержания», можно в справочной литературе текстового редактора. В название и в заголовках не должно присутствовать точек. При представлении Заказчиком методических указаний своего учебного заведения на оформление Работы, оформление должно полностью соответствовать его содержанию.</li>
                                <li>Введение — не менее 1-го печатного листа и содержать: цели, задачи, актуальность объекта, методы и степень изучаемой проблемы.</li>
                                <li>Основная часть — ярко выраженная структура с единообразными заголовками глав, с оптимизированным использованием пробелов, отступов и абзацев. Каждая глава начинается с новой страницы, и заканчивается выводом по данной главе. А в конце всей Работы общий вывод.</li>
                                <li>Оформление сносок на цитируемые источники должно иметь вид указанный в Заказе. Ссылки необходимо делать на все цитируемые материалы, в виде сносок в скобках. Необходимо через запятую указывать списочный номер источника и номер страницы. Обязательно должна присутствовать следующая информация: ФИО Исполнителя, название источника, год и место выпуска.</li>
                                <li>Нумерация страниц — начинается с титульного листа, но на нём номер не ставится, лишь название Работы.</li>
                                <li>Оформление вставок — графика, рисунок, схема, таблица, недопустимо использовать в отсканированном виде, только единообразные и подписанные. Каждая вставка в Работе должна иметь краткое пояснение и вывод по представленной информации.</li>
                                <li>
                                    Заключение — объём не менее 1 печатной страницы, Исполнитель должен сделать итоговый вывод по Работе и представленной в ней проблеме, и высказать свои рекомендации по её решению.
                                </li>
                                <li>Список литературы — расположен на отдельном, пронумерованном листе, и должен содержать все используемые источники. Для простой Работы — не мене 5, для курсовой — не менее 10, для диплома — не менее 40. Рекомендовано использование литературы, в качестве источников, последних лет (после 2010г). Оформление списка допускается как в алфавитном порядке, так и по степени важности: законы, подзаконные акты, указы, положения, монографии, учебники, журналы, статьи, интернет-сайты.</li>
                                <li>Приложения — оформляются после основной части Работы, каждое приложение на отдельном листе и с заголовком.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection