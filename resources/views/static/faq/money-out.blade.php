@extends('layouts.default')

@section('content')
    @include('parts.breadcrumbs', ['title' => 'О проекте', 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('faq'), 'name' => 'Часто задаваемые вопросы']
        )
    ])
    <section>
        <div class="container pb-9">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body font-size-1">
                            <h3>Вывод средств</h3>
                            <p>В данной статье Вы сможете найти всю информацию о процедуре вывода средств с сайта.</p>
                            <p>Положение о выводе средств с сайта.</p>
                            <p>Согласно Пользовательскому Соглашению: </p>
                            <p>В случае если Заказчик принял решение произвести возврат денежных средств из Личного кабинета на Платформе на свой расчетный счет, Заказчик соглашается осуществить вывод таких денежных средств стандартными способами вывода, предложенными на Платформе.</p>
                            <p>Что мне сделать, чтобы вывести средства с сайта?</p>
                            <p>Для вывода средств с сайта, Вам необходимо навести курсор мыши на аватар (справа вверху сайта) и, в открывшемся меню, нажать кнопку "Баланс". Перейдите на страницу баланса, нажмите кнопку "ВЫВЕСТИ СРЕДСТВА"</p>
                            <p>Затем укажите сумму:</p>
                            <p>Доступные способы вывода средств. Выберете удобный способ вывода:</p>
                            <p>в зависимости от Вашей локации и срока вывода средств Вам будут предложены те или иные способы вывода*.</p>
                            <p>*Для обеспечения мер по борьбе с легализацией средств, полученных незаконным путем, в течение месяца Заказчик может выводить средства, внесенные через карту, также только на карту.</p>
                            <p>В случае, если за последний месяц средства с сайта не вводились, доступны все способы вывода.</p>
                            <p>В остальных случаях вывод средств доступен только тем способом, который за последний календарный месяц по сумме начисленных на счет средств составил 50% от всей суммы пополнения или более.</p>
                            <p>и заполните данные, которые запросит система:</p>
                            <p> - для вывода на банковские карты: Номер карты, Держатель (как на карте), ФИО;</p>
                            <p> - для вывода на QIWI: Номер кошелька Qiwi (10 цифр);</p>
                            <p> - для вывода на WebMoney: Номер кошелька WMR (12 цифр);</p>
                            <p> - для вывода на Яндекс.Кошелек: Номер кошелька, ФИО;</p>
                            <p> - для вывода на ePayments: Ваш аккаунт ePayments (email).</p>
                            <p>и нажмите на кнопку "СОЗДАТЬ ЗАЯВКУ"</p>

                            <p>После того как Вы введете все данные и нажмете на кнопку "Создать заявку", на номер Вашего мобильного телефона будет выслан sms-код безопасности для подтверждения заявки на вывод средств. Если по технической причине Вы не можете получить sms-код для подтверждения заявки на вывод средств, на сайте есть возможность заменить ввод sms-кода на ввод пин-кода.</p>
                            <p>Комиссия платежного агрегатора за вывод средств.</p>
                            <p>Наша компания является официально зарегистрированным лицом, и вся работа ведется в соответствии с законодательством, которое обязывает нас пополнять и выводить средства пользователей через аккредитованные платежные системы. Платежная система вправе самостоятельно устанавливать и взимать комиссию в момент пополнения баланса или в момент вывода средств за проведение финансовой операции. Сервис ЭКСПЕРТ24 не получает доход от операций по зачислению и списанию средств, и не может влиять на процент комиссии на ввод и вывод, т.к. она взимается сторонней организацией.</p>
                            <p>Ограничения на минимальные и максимальные суммы вывода средств.</p>
                            <p>Обратите внимание, платежной системой установлены ограничения на минимально и максимально допустимые суммы вывода средств.</p>
                            <p>Для граждан Российской Федерации.</p>
                            <p>на банковские карты: минимум - 650 рублей, максимум - 13 000 рублей.</p>
                            <p>на электронные кошельки: минимум - 150 рублей, максимум - 13 000 рублей.</p>
                            <p>Для граждан СНГ и других стран.</p>
                            <p>на банковские карты: минимум - 800 рублей, максимум - 13 000 рублей.</p>
                            <p>на электронные кошельки: минимум - 150 рублей, максимум - 13 000 рублей.</p>
                                 
                            <p>Сроки рассмотрения заявки на вывод средств.</p>
                            <p>Эта информация указывается при создании каждой заявки. </p>
                            <p>Заявки на вывод средств одобряются на третий рабочий день, не считая дня подачи заявки. В день одобрения заявки, она обрабатывается в порядке очереди, с 10.00 до 20.00 по московскому времени. После одобрения заявки производится перевод средств. Время перевода зависит от способа вывода и занимает от нескольких минут, до пяти банковских дней. Данная информация указана при создании каждой заявки.</p>
                            <p>Срок обработки заявок на выплату денежных средств в течении трех рабочих дней установлен для целей обнаружения и предотвращения финансовых и противоправных действий на Платформе - мы предотвращаем «легализацию денежных средств полученных преступным путем». Наши специалисты проверяют подозрительные заявки на выплату средств и имеют возможность обнаружить противоправные действия.</p>
                            <p>Вы можете создавать неограниченное кол-во заявок ежедневно, в соответствии с Вашем балансом. Временной интервал между созданием заявок должен быть равен не менее 5 (пяти) минут.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection