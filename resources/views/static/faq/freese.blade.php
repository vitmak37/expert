@extends('layouts.default')

@section('content')
    @include('parts.breadcrumbs', ['title' => 'О проекте', 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('faq'), 'name' => 'Часто задаваемые вопросы']
        )
    ])
    <section>
        <div class="container pb-9">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body font-size-1">
                            <h3>Заморозка денежных средств</h3>
                            <p>Уважаемый Заказчик, в этой статье можно найти ответы на вопросы по заморозке средств.</p>
                            <p>А) Причины заморозки денежных средств.</p>
                            <p>Согласно пункту 17 Общего Положения Пользовательского Соглашения:</p>
                            <p>В целях обеспечения безопасности средств на счетах пользователей, Платформа оставляет за собой право заморозить средства на счетах Заказчик или Исполнитель. Возврат средств на счет происходит в течение 3 (трёх) рабочих дней после обращения в службу поддержки посредством электронной почты.</p>
                            <p>Информация о замороженных средствах есть в Вашем Личном Кабинете, в разделе "Баланс":</p>
                            <p>Заморозка денежных средств на счету Пользователя происходит в случае, если в течение длительного времени Пользователь не вывел и не использовал денежные средства со своего Баланса в Личном Кабинете на сайте.</p>
                            <p>Б) Как я могу разморозить денежные средства?</p>
                            <p>Возврат средств на счет происходит в течение 3 (трёх) рабочих дней после обращения в службу поддержки посредством электронной почты. Отправить запрос на разморозку средств можно на электронный адрес Службы Поддержки ____________</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection