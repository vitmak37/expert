@extends('layouts.default')

@section('content')
    @include('parts.breadcrumbs', ['title' => 'О проекте', 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('faq'), 'name' => 'Часто задаваемые вопросы']
        )
    ])
    <section>
        <div class="container pb-9">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body font-size-1">
                            <h3>Часто задаваемые вопросы</h3>
                            <ul>
                                <li><a href="{{ route('faq.pin') }}">Пин-код</a></li>
                                <li><a href="{{ route('faq.money-out') }}">Вывод средств</a></li>
                                <li><a href="{{ route('faq.account') }}">Изменение и удаление аккаунта</a></li>
                                <li><a href="{{ route('faq.cost') }}">Стоимость работы на сайте</a></li>
                                <li><a href="{{ route('faq.what') }}">Чем занимается компания Эксперт24</a></li>
                                <li><a href="{{ route('faq.freese') }}">Заморозка денежных средств</a></li>
                                <li><a href="{{ route('faq.fee') }}">Комиссия биржи для Заказчиков</a></li>
                                <li><a href="{{ route('faq.payment') }}">Как оплатить</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection