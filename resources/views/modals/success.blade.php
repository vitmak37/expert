@extends('modals.base')

@section('modalId'){{ 'successModal' }}@overwrite

@section('modalContent')
    <div class="card-body">
        <div class="d-flex justify-content-center mb-5">
            <div class="rounded-icon">
                <img src="{{ asset('img/check.svg') }}" alt="">
            </div>
        </div>
        <div class="text-center">
            <h4>{{ $successTitle }}</h4>
            <p class="text-muted">{{ $successMessage }}</p>
        </div>
    </div>
@overwrite