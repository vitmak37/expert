@extends('modals.base')

@section('modalId'){{ 'changePasswordModal' }}@overwrite

@section('modalContent')
    <div class="card-body">
        <div class="auth-card__title-section d-flex justify-content-between mb-5">
            <h4 class="card__title text-left">Смена пароля</h4>
        </div>
        <form method="POST" action="{{ route('users.updatepw') }}">
            @csrf
            <div class="col-12 p-0 form-group">
                <label for="oldpassword">Старый пароль</label>
                <input type="password" class="form-control" name="oldpassword" id="oldpassword">
            </div>

            <div class="col-12 p-0 form-group">
                <label for="newpassword">Новый пароль</label>
                <input type="password" class="form-control" name="newpassword" id="newpassword">
            </div>


            <div class="col-12 p-0 mt-4">
                <button type="submit" class="btn btn-blue btn-full" id="changePassword">Сменить пароль</button>
            </div>
        </form>
    </div>
@overwrite