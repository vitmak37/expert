@extends('modals.base')

@section('modalId'){{ 'safecrowCardModal' }}@overwrite

@section('modalContent')
    <div class="card-header border-0 pt-5 pl-5 pr-5 flex-column align-items-start mb-3">
        <h4>Привязка карты Safecrow</h4>
        <p class="text-muted-dark">Вы должны привязать валидную карту к вашему аккаунту safecrow, без этого мы не сможем создать заказ или добавить отклик:( Помните, мы не храним эту информацию на своих серверах!</p>
        <p class="text-muted">Если вы уже привязали карту и видите это сообщение снова, обновите страницу - мы получаем данные через несколько секунд</p>
    </div>
    <div class="card-body">
        <a href="{{ auth()->user()->getCardAssignUrl() }}" class="btn btn-primary">Перейти к привязке карты</a>
    </div>
@overwrite