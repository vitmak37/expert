@extends('modals.base')

@section('modalId'){{ 'authorizationModal' }}@overwrite

@section('modalContent')
    <div class="card-body">
        <div class="auth-card__title-section d-flex justify-content-between mb-6">
            <h4 class="card__title text-left">Вход</h4>
            <span class="change-modal">Еще не с нами? <a href="#" id="changeLogin">Регистрация</a></span>
        </div>
        <form onsubmit="return false;" action="">
            <div class="col-12 p-0 mt-4">
                <label for="auth_login">Электронная почта</label>
                <input type="text" id="auth_login" name="login" placeholder="mail@example.com"
                       class="form-control">
                <span id="auth_login_invaliddescription" class="invalid-feedback"></span>
            </div>
            <div class="col-12 p-0 mt-3">
                <label for="auth_password">Пароль</label>
                <input type="password" id="auth_password" name="password" placeholder="Пароль" class="form-control">
                <span id="auth_login_invalidpwdescription" class="invalid-feedback"></span>
            </div>
            <div class="col-12 p-0 mt-3">
                <label for="remember" class="custom-control custom-checkbox">
                    <input type="checkbox" id="remember" name="remember" class="custom-control-input">
                    <span class="custom-control-label">
                        Запомнить меня
                    </span>
                </label>
            </div>
            <div class="col-12 p-0 mt-5">
                <button type="submit" class="btn btn-primary btn-block" id="loginSubmit">Вход</button>
            </div>
            <div class="col-12 p-0 text-center mt-3">
                <a href="#">Я забыл пароль</a>
            </div>

        </form>

        <div class="col-12 form__divider mt-3 p-0">
            <span>Или</span>
        </div>

        <div class="vue" id="auth-vue">
            <social-auth
                    :_soc_login="'{{ route('oauth.login') }}'"
                    :_soc_user="'{{ route('oauth.user') }}'"
            ></social-auth>
        </div>

        {{--@include('modals.parts.social')--}}
    </div>
@overwrite