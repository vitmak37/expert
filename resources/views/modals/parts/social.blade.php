<div class="social__entrant">
    <div class="text-center social-entrant__title">Войдите с помощью учетной записи в социальных сетях</div>
    <div class="d-flex mt-4 justify-content-center">
        <a href="#" class="social__link social__link_vk"><i class="fa fa-vk"></i></a>
        <a href="#" class="social__link social__link_fb"><i class="fa fa-facebook"></i></a>
        <a href="#" class="social__link social__link_ig"><i class="fa fa-instagram"></i></a>
    </div>
</div>