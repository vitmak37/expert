@extends('modals.base')

@section('modalId'){{ 'registerModal' }}@overwrite

@section('modalContent')
    <div class="card-body">
        <div class="auth-card__title-section d-flex justify-content-between">
            <h4 class="card__title text-left">Регистрация</h4>
            <span class="change-modal">Уже зарегистрированы? <a href="#" id="changeRegister">Войти</a></span>
        </div>
        <form onsubmit="return false" action="">
            <div class="col-12 p-0 mt-4">
                <label for="login">Имя пользователя</label>
                <div class="question-mark">
                    <span class="button">?</span>
                    <div class="hidden__text">Логин вводится на русском языке и используется для обращения к вам на сайте
                    </div>
                </div>
                <input  type="text" id="login" name="login" placeholder="Как к вам обращаться?"
                       class="form-control">
                <span id="login_invaliddescription" class="invalid-feedback"></span>
            </div>
            <div class="col-12 p-0 mt-3">
                <label for="email">E-mail</label>
                <input  type="email" id="email" name="email" placeholder="example@example.com"
                       class="form-control">
                <span id="email_invaliddescription" class="invalid-feedback"></span>
            </div>
            <div class="col-12 p-0 mt-3">
                <label for="password">Пароль</label>
                <input  type="password" id="password" name="password" placeholder="Пароль"
                       class="form-control">
                <span id="password_invaliddescription" class="invalid-feedback"></span>
            </div>
            <div class="col-12 p-0 mt-3">
                <label for="password_confirm">Повторите пароль</label>
                <input type="password" id="password_confirm" name="password_confirm"
                       placeholder="Подтверждение пароля"
                       class="form-control">
                <span id="password_confirmation_invaliddescription" class="invalid-feedback"></span>
            </div>
            <div class="col-12 p-0 mt-3">
                <label for="terms" class="custom-control custom-checkbox">
                    <input type="checkbox" id="terms" name="terms" class="custom-control-input">
                    <span class="custom-control-label">
                        Согласен с "<a href="{{ route('privacy-policy') }}">правилами обработки информации</a>", а также <a
                                href="https://www.safecrow.ru/term">условиями использования сервиса SafeCrow</a>
                    </span>
                </label>
            </div>
            <div class="col-12 p-0 mt-3">
                <button type="submit" class="btn btn-blue btn-full" id="registerSubmit">Создать аккаунт</button>
            </div>

{{--            @include('modals.parts.social')--}}
        </form>

        <div class="col-12 form__divider mt-3 p-0">
            <span>Или</span>
        </div>

        <div id="register-vue">
            <social-auth
                :_soc_login="'{{ route('oauth.login') }}'"
                :_soc_user="'{{ route('oauth.user') }}'"
            ></social-auth>
        </div>
    </div>
@overwrite