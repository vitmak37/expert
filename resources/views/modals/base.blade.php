<div class="modal" tabindex="-1" role="dialog" id="@yield('modalId')">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            @section('modalContent')
                @show
        </div>
    </div>
</div>