@extends('modals.base')

@section('modalId'){{ 'setNewImage' }}@overwrite

@section('modalContent')
    <div class="card-header border-0 pt-5 pl-5 pr-5 flex-column align-items-start mb-3">
        <h4>Установка стоимости заказа</h4>
    </div>
    <div class="card-body">
        <form action="{{ route('profile.upload') }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div >
                <div class="attachment">
                    <input id="attachment" multiple type="file" name="avatar" class="attachment__file">
                    <label for="attachment"><i class="fa fa-paperclip"></i> Выбрать фотографию</label>
                </div>
            </div>
            <div class="ml-2">
                <button type="submit" class="btn btn-outline-primary">Загрузить</button>
            </div>
        </form>
    </div>
@overwrite

