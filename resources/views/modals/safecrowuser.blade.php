@extends('modals.base')

@section('modalId'){{ 'safecrowUserModal' }}@overwrite

@section('modalContent')
    <div class="card-header border-0 pt-5 pl-5 pr-5 flex-column align-items-start mb-3">
        <h4>Создание аккаунта Safecrow</h4>
        <small class="text-muted-dark">Для работы с нашим сайтом, вы должны создать аккаунт в гарант-сервисе <a href="http://safecrow.ru/howworks" rel="nofollow">Safecrow</a>. Для этого, пожалуйста, заполните форму ниже.</small>
    </div>
    <div class="card-body">
        <form action="{{ route('users.safecrowprofile', ['id' => auth()->user()->id]) }}" method="POST">
            @csrf


            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group">
                <label for="name">Имя и фамилия</label>
                <input type="text" required name="name" class="form-control {{ $errors->has('name') ?  'is_invalid' : ''}}" placeholder="Иван Иванов">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" required name="email" class="form-control {{ $errors->has('email') ?  'is_invalid' : ''}}" value="{{ auth()->user()->email }}" placeholder="mail@example.com">
            </div>

            <div class="form-group">
                <label for="phone">Номер мобильного телефона</label>
                <input type="tel" required name="phone" class="form-control {{ $errors->has('phone') ?  'is_invalid' : ''}}" placeholder="+79118888888">
            </div>

            <small class="text-muted">
                Важно! Мы не сохраняем данную информацию у себя и не отображаем ее никому.
                Данная информация хранится на серверах Safecrow и используется для работы гарант-сервиса.
            </small>

            <small class="text-muted">
                Нажимая кнопку ниже вы соглашаетесь с <a href="https://www.safecrow.ru/term">условиями использования сервиса Safecrow</a>.
            </small>
            <div class="mt-4">
                <button class="btn btn-primary">Подтвердить и создать аккаунт</button>
            </div>
        </form>
    </div>
@overwrite