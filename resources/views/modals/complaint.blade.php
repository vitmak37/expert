@extends('modals.base')

@section('modalId'){{ 'complaintModal' }}@overwrite

@section('modalContent')
    <div class="card-header border-0 pt-5 pl-5 pr-5">
        <h4>Жалоба на заказ</h4>
    </div>
    <div class="card-body">
        <form action="{{ route('complaint.store') }}" method="POST" class="d-flex flex-wrap">
            @csrf

            <input type="hidden" name="order_id" id="complaintModalOrderid">

            <div class="col-12 p-0 mt-4">
                <label for="complaintModalComment">Текст жалобы</label>
                <textarea name="comment" id="complaintModalComment" rows="10" class="form-control"></textarea>
            </div>
            <div class="mt-4">
                <button class="btn btn-primary">Пожаловаться</button>
            </div>
        </form>
    </div>
@overwrite