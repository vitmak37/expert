@extends('modals.base')

@section('modalId'){{ 'respondModal' }}@overwrite

@section('modalContent')
    <div class="card-header border-0 pb-0 pt-5 pr-5 pl-5">
        <span class="h4">Заявка на заказ #<span id="orderIdName"></span></span>
    </div>
    <div class="card-body pt-0">
        <form action="{{ route('responds.store') }}" method="POST" class="d-flex flex-wrap">
            @csrf

            <input type="hidden" name="order_id" id="orderId">

            <div class="col-6 pl-0 mt-4">
                <label for="respondModal-budget">Бюджет</label>
                <input type="number" min="100" id="respondModal-budget" name="budget" placeholder="Бюджет"
                       class="form-control">
            </div>
            <div class="col-6 pr-0 custom-control custom-checkbox mt-4 d-flex align-items-center pt-5">
                <input type="checkbox" name="discus" id="discus" class="custom-control-input">
                <label for="discus" class="custom-control-label cursor-pointer">
                    <span>По договоренности</span>
                </label>
            </div>
            <div class="col-12 p-0 mt-4">
                <label for="respondModal-comment">Сопроводительный комментарий</label>
                <textarea name="comment" id="respondModal-comment" cols="30" rows="10" class="form-control"></textarea>
            </div>
            <div class="mt-4">
                <button class="btn btn-primary">Добавить отклик</button>
            </div>
        </form>
    </div>
@overwrite