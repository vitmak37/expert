@extends('modals.base')

@section('modalId'){{ 'setCostModal' }}@overwrite

@section('modalContent')
    <div class="card-header border-0 pt-5 pl-5 pr-5 flex-column align-items-start mb-3">
        <h4>Установка стоимости заказа</h4>
    </div>
    <div class="card-body">
        <form action="{{ route('responds.setcost', ['id' => $respondId]) }}" method="POST">
            @csrf

            <div class="form-group">
                <label for="budget">Стоимость выполнения</label>
                <input type="number" required name="budget" class="form-control {{ $errors->has('budget') ?  'is_invalid' : ''}}" placeholder="6500" min="100">
            </div>

            <div class="mt-4">
                <button class="btn btn-primary">Установить стоимость</button>
            </div>
        </form>
    </div>
@overwrite