@extends('modals.base')

@section('modalId'){{ 'acceptRespond' }}@overwrite

@section('modalContent')
    <div class="card-header border-0 pt-5 pl-5 pr-5 flex-column align-items-start mb-3">
        <h4>Подтвердите свое согласие с условиями заказа</h4>
        <h6 class="mt-3">Стоимость заказа - {{ $order->winner->budget }}</h6>
    </div>
    <div class="card-body">
        <form action="{{ route('responds.accept', ['id' => $order->winner->id]) }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="card_id">Карта для выплаты заказа</label>
                <select name="card_id" id="card_id" class="form-control js-selectize">
                    @foreach(auth()->user()->getListOfUserCards() as $card)
                        <option value="{{ $card->id }}">{{ $card->card_number }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mt-4">
                <button class="btn btn-primary">Согласиться</button>
            </div>
        </form>
    </div>
@overwrite