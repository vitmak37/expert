@extends('modals.base')

@section('modalId'){{ 'errorModal' }}@overwrite

@section('modalContent')
    <div class="card-body">
        <div class="d-flex justify-content-center mb-5">
            <div class="rounded-icon bg-red-lightest">
                <img src="{{ asset('img/cross-big.svg') }}" alt="">
            </div>
        </div>
        <div class="text-center">
            <h4>{{ $errorTitle }}</h4>
            <p class="text-muted">{{ $errorMessage }}</p>
        </div>
    </div>
@overwrite