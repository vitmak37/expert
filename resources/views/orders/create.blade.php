@extends('layouts.default')


@section('content')
    @include('parts.breadcrumbs', ['title' => 'Биржа заказов', 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('orders'), 'name' => 'Биржа заказов'],
            ['link' => route('orders.create'), 'name' => 'Создание заказа']
        )
    ])

    @if($errors->any())
        <div class="row justify-content-center">
            <div class="alert alert-danger col-8">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <div class="row justify-content-center m-0">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header border-0 p-5">
                    <div class="card__title">Создать заказ</div>
                </div>
                <div class="card-body mt-0 pt-0">
                    <form action="{{ route('orders.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-12 mt-0">
                                <label for="title">Название работы</label>
                                <input type="text" id="title" value="{{ old('title') }}" name="title" placeholder="Название"
                                       class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" required>
                            </div>
                            <div class="col-12 mt-3">
                                <label for="description">Комментарий к работе</label>
                                <textarea name="description" id="description" placeholder="Введите комментарий к работе" required
                                          class="response__textarea form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" rows="5">{{ old('description') }}</textarea>
                            </div>
                            <div class="col-md-6 col-12 mt-3">
                                <label for="work_amount">Количество страниц</label>
                                <input type="text" id="work_amount" value="{{ old('work_amount') }}" required name="work_amount" placeholder="Количество страниц"
                                       class="form-control  {{ $errors->has('work_amount') ? 'is-invalid' : '' }}">
                            </div>

                            <div class="col-md-6 col-12 mt-3">
                                <label for="deadline">Предельный срок сдачи</label>
                                <input type="text" id="deadline" value="{{ old('deadline') }}" required name="deadline" placeholder="23/12/2018"
                                       class="form-control datepicker  {{ $errors->has('deadline') ? 'is-invalid' : '' }}" autocomplete="off">
                            </div>

                            <div class="col-md-6 col-12 mt-3">
                                <label for="subject">Дисциплина</label>
                                <div class="select-input">
                                    <select name="subject" required id="subject" class="form-control  {{ $errors->has('subject') ? 'is-invalid' : '' }}">
                                        @foreach($subjects as $subject)
                                            <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-12 mt-3">
                                <label for="work_type">Тип работы</label>
                                <div class="select-input">
                                    <select name="work_type" id="work_type" class="form-control  {{ $errors->has('work_type') ? 'is-invalid' : '' }}" required>
                                        @foreach($types as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-12 mt-3">
                                <label for="uniqness_service_id">Сервис антиплагиата</label>
                                <div class="select-input">
                                    <select name="uniqness_service_id" id="uniqness_service_id" class="form-control  {{ $errors->has('uniqness_service_id') ? 'is-invalid' : '' }}">
                                        @foreach($uniqueness_services as $service)
                                            <option value="{{ $service->id }}">{{ $service->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-12 mt-3">
                                <label for="uniqness_service_percent">Процент уникальности</label>
                                <input type="text" id="uniqness_service_percent" value="{{ old('uniqness_service_percent') }}" name="uniqness_service_percent" placeholder="65"
                                       class="form-control  {{ $errors->has('uniqness_service_percent') ? 'is-invalid' : '' }}" required>
                            </div>
                            <div class="col-md-6 col-12 mt-3">
                                <label for="budget">Бюджет</label>
                                <input type="text" id="budget" value="{{ old('budget') }}" name="budget" placeholder="650"
                                       class="form-control {{ $errors->has('budget') ? 'is-invalid' : '' }}" required min="100">
                            </div>
                            <div class="col-md-6 col-12 mt-3">
                                <label>Прикрепить файлы</label>
                                <div class="attachment text-center attachment-input">
                                    <input id="attachment" multiple type="file" name="files[]" class="attachment__file">
                                    <label for="attachment"><i class="fa fa-paperclip"></i> Прикрепить файлы</label>
                                </div>
                            </div>
                            <div class="mt-3 pl-1 col-12">
                                <button class="btn btn-full btn-blue mt-4 mr-2 ml-2">Разместить заказ</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if (auth()->check())
        @if(auth()->user()->type && !auth()->user()->hasSafecrowAccount())
            @include('modals.safecrowuser')
        @endif
    @endif

    @if(auth()->check() && auth()->user()->hasSafecrowAccount() && !auth()->user()->hasAssignedCard())
        @include('modals.safecrowcardassign')
    @endif


@endsection