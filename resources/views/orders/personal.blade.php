@extends('layouts.default')

@section('content')
    @include('parts.breadcrumbs', ['title' => 'Мои заказы', 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('orders'), 'name' => 'Биржа заказов'],
            ['link' => route('orders.personal'), 'name' => 'Мои заказы']
        )
    ])

    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-8 order-1 order-lg-0">
                <div class="card">
                    <div class="card-header p-5 pb-0 card-header_small-bottom border-0">
                        <div class="card__title">Мои заказы</div>
                    </div>
                    <div class="card-body o-card p-3 p-sm-5">
                        @foreach($orders as $order)
                            <div class="order__block">
                                <div class="border-bottom pb-5 row align-items-center mb-5">
                                    <div class="order__info col-lg-6">
                                        <div class="order__subject-block ">
                                            <div class="order__subject"><a href="{{ route('orders.show', ['id' => $order->id]) }}" class="h4">{{ $order->title }}</a></div>
                                        </div>
                                        <div class="order__categories d-flex">
                                            <div class="order__deadline mr-3">{{ $order->deadline }}</div>
                                            <p>{{ $order->subjects->implode('name', ',') }}</p>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center col-lg-6">
                                        <div class="order__responds col-lg-6">
                                            <span>{{ $order->responds()->count() }}</span> Ставок
                                        </div>
                                        <div class="order__status col-lg-6">
                                            <span class="ml-2 tag tag-{{ $order->statusColor() }}">{{ $order->printableStatus() }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection