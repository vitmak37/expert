@extends('layouts.default')

@section('content')
    @include('parts.breadcrumbs', ['title' => 'Просмотр заказа #' . $order->id, 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('orders'), 'name' => 'Биржа заказов'],
            ['link' => route('orders.show', ['id' => $order->id]), 'name' => 'Заказ #' . $order->id]
        )
    ])

    <div class="container">
        <div class="row">
            <div class="col-lg-8 order-1 order-lg-0">
                <div class="card">
                    <div class="card-header p-7 border-0">
                        <div class="card__title">{{ $order->title }} <span class="ml-2 tag tag-{{ $order->statusColor() }}">{{ $order->printableStatus() }}</span></div>
                    </div>

                    @if($order->status == \App\Order::STATUS_BANNED)
                        <div class="card-body pt-0">
                            Данный заказ был заблокирован / удален. Причина: {{ $order->ban_reason }}
                        </div>
                    @elseif($order->status == \App\Order::STATUS_AUCTION)
                        @include('orders.parts.auction')
                    @else
                        @include('orders.parts.else')
                    @endif

                </div>
            </div>

            @include('orders.parts.order-details')
        </div>
    </div>
    @if(auth()->check())
        @include('modals.respond')
        @include('modals.complaint')

        @if(auth()->user()->hasSafecrowAccount())
            @if(!auth()->user()->hasAssignedCard())
                @include('modals.safecrowcardassign')
            @endif
        @endif

        @if($order->status == \App\Order::STATUS_ACCEPTANCE)
            @include('modals.setcost', ['respondId' => $order->winned_respond])
            @include('modals.acceptRespond')
        @endif

    @endif
@endsection

@section('scripts')
    {{--<script defer>--}}
        {{--document.addEventListener('DOMContentLoaded', function () {--}}
            {{--$('#safecrowCardModal').modal('show');--}}
        {{--});--}}
    {{--</script>--}}
@append