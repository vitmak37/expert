@extends('layouts.default')

@section('content')
    @include('parts.breadcrumbs', ['title' => 'Биржа заказов', 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('orders'), 'name' => 'Биржа заказов']
        )
    ])

    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        @if($orders->count() > 0)
                            <div class="wrap orders_wrap">
                                <div>
                                    @foreach($orders as $order)
                                        <div class="card__subcard">
                                            <div class="d-flex align-items-center">
                                                <div class="info__element col p-0 text-muted">
                                                    <i class="fa fa-clock-o mr-2"></i>
                                                    {{ $order->created_at->format('Y-m-d H:i') }}
                                                </div>
                                                <div class="info__element col p-0 text-muted">
                                                    <i class="fa fa-calendar-o mr-2"></i>
                                                    до {{ $order->deadline->format('Y-m-d H:i') }}
                                                </div>
                                                <div class="info__element col p-0 text-muted">
                                                    <span class="fa-icon mr-2">ID</span>
                                                    {{ $order->id }}
                                                </div>
                                                <div class="cost">
                                                    {{ $order->budget }}
                                                </div>
                                            </div>

                                            <div class="order__info">
                                                <div class="order__title">
                                                    <a href="{{ route('orders.show', ['id' => $order->id]) }}">{{ $order->title }}</a>
                                                </div>
                                                <div class="order__categories order__categories_long">
                                                    @foreach($order->subjects as $subject)
                                                        <span>{{ $subject->name }}</span>
                                                    @endforeach
                                                </div>
                                            </div>

                                            <div class="form__divider form__divider_dark mt-4 cursor-pointer js-hidden-open">
                                                <span>Подробнее <i class="fa fa-chevron-down"></i></span>
                                            </div>

                                            <div class="hidden__info">
                                                <div class="subcard__info mt-3">
                                                    <div class="col-lg-4 d-flex flex-column p-0">
                                                        <div class="text-default mb-4">
                                                            Заказчик
                                                        </div>
                                                        <div class="d-flex align-items-center">
                                                            <div class="response__image mr-3">
                                                                <span class="avatar"
                                                                      style="background-image: url('{{ $order->user->avatar_url }}')"></span>
                                                            </div>
                                                            <div class="response__profile-info">
                                                                <div class="profile-info__name">{{ $order->user->username }}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-8 d-flex flex-column p-0">
                                                        <div class="text-default mb-4">
                                                            Информация о заказе
                                                        </div>
                                                        <div>
                                                            <div class="response__commentary">
                                                                <p>{{ $order->description }}</p>
                                                            </div>
                                                            <div class="subcard__info mt-3">
                                                                <div class="d-flex mr-3">
                                                                    <div class="point-icon mr-2">
                                                                        <img src="/img/pages.svg" alt="">
                                                                    </div>
                                                                    <div class="point-description">
                                                                        {{ $order->work_amount }} страниц
                                                                    </div>
                                                                </div>
                                                                <div class="response__documents">
                                                                    @foreach($order->files as $file)
                                                                        <div>
                                                                            <span class="document__icon">
                                                                                <img src="/img/file.svg" alt="">
                                                                            </span>
                                                                            <a download
                                                                               href="{{ Storage::url($file->path) }}"
                                                                               class="document__name">{{ $file->original_name }}
                                                                            </a>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            @if(auth()->check())
                                                                <div class="subcard__info mt-3 align-items-center">
                                                                    @if(auth()->user()->type == 'executant' &&!auth()->user()->addedRespond($order->id))
                                                                        <button type="button"
                                                                                class="btn btn-primary mr-4"
                                                                                data-toggle="modal"
                                                                                @if(!auth()->user()->sc_user_id)
                                                                                data-target="#safecrowUserModal"
                                                                                @elseif(!auth()->user()->hasAssignedCard())
                                                                                data-target="#safecrowCardModal"
                                                                                @else
                                                                                data-target="#respondModal"
                                                                                @endif
                                                                                data-orderid="{{ $order->id }}">Оставить
                                                                            заявку
                                                                        </button>
                                                                    @endif

                                                                    <a href="#" class="text-danger" data-toggle="modal"
                                                                       data-target="#complaintModal"
                                                                       data-orderid="{{ $order->id }}"><i
                                                                                class="fa fa-exclamation-triangle"></i>
                                                                        Пожаловаться</a>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <div class="card-body pt-0">
                                <span>К сожалению, заказов подходящих вашему запросу не найдено:(</span>
                            </div>
                        @endif
                    </div>
                    <div class="card-footer">
                        {{ $orders->links() }}
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <form method="GET">
                    <div class="card">
                        <div class="card-header card-header_border d-flex justify-content-between align-items-center">
                            <div class="card__title card__title_small">Фильтр заказов</div>
                            <div class="card__discard"><a href="{{ route('orders') }}">сбросить</a></div>
                        </div>

                        <div class="card-body">
                            <div class="search__input form-group position-relative">
                                <input type="text" class="form-control" placeholder="Введите тему работы"
                                       value="{{ request('q') }}" name="q">
                                <button class="search__button"><i class="fa fa-search"></i></button>
                            </div>

                            <div class="form-group position-relative">
                                <label for="work_type">Выберите тип работы</label>
                                <select type="text" class="form-control js-selectize" name="w[]" id="work_type" multiple
                                        placeholder="Тип работы">
                                    @foreach($workTypes as $workType)
                                        <option value="{{ $workType->id }}" {{ in_array($workType->id, request('w') ?? []) ? 'selected' : '' }}>{{ $workType->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group position-relative">
                                <label for="work_type">Выберите область исследования</label>
                                <select type="text" class="form-control js-selectize" name="s[]" id="work_type" multiple
                                        placeholder="Область работы">
                                    @foreach($subjects as $subject)
                                        <option value="{{ $subject->id }}" {{ in_array($subject->id, request('s') ?? []) ? 'selected' : '' }}>{{ $subject->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div>
                            <button type="submit" class="btn btn-primary btn-block">Фильтровать</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(auth()->check())
        @include('modals.respond')
        @include('modals.complaint')

            @if(auth()->user()->type && !auth()->user()->hasSafecrowAccount())
                @include('modals.safecrowuser')
            @endif

        @if(auth()->user()->hasSafecrowAccount())
            @if(!auth()->user()->hasAssignedCard())
                @include('modals.safecrowcardassign')
            @endif
        @endif
    @endif
@endsection

@section('scripts')
    {{--<script>--}}
        {{--$('#safecrowCardModal').modal('show');--}}
    {{--</script>--}}
@endsection