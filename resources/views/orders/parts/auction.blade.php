<div class="card__subcard">
    <div class="subcard__info mt-3 row">
        <div class="col-lg-4 d-flex flex-column">
            <div class="text-default mb-4">Заказчик</div>
            <div class="d-flex align-items-center">
                <div class="response__image">
                    <span class="avatar mr-2" style="background-image: url('{{ $order->user->avatar_url }}');"></span>
                </div>
                <div class="response__profile-info">
                    <div class="profile-info__name">{{ $order->user->username }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 d-flex flex-column">
            <div class="text-default mb-4">
                Описание заказа
            </div>
            <div>
                <div class="response__commentary">
                    <p>{{ $order->description }}</p>
                </div>
                <div class="subcard__info mt-3">
                    <div class="d-flex mr-3">
                        <div class="point-icon mr-2">
                            <img src="/img/pages.svg" alt="">
                        </div>
                        <div class="point-description">
                            {{ $order->work_amount }} страниц
                        </div>
                    </div>
                    <div class="response__documents">
                        @foreach($order->files as $file)
                            <a href="#">
                                                                    <span class="document__icon"><img
                                                                                src="/img/file.svg" alt=""></span>
                                <a download href="{{ Storage::url($file->path) }}"
                                   class="document__name">{{ $file->original_name }}</a>
                            </a>
                        @endforeach
                    </div>
                </div>
                @if(auth()->check())
                    <div class="subcard__info mt-4 align-items-center">
                        @if(auth()->user()->type == 'executant' && !auth()->user()->addedRespond($order->id) && $order->status == \App\Order::STATUS_AUCTION)
                            <button type="button" class="btn btn-primary mr-4" data-toggle="modal"
                                    @if(!auth()->user()->sc_user_id || !auth()->user()->hasAssignedCard())
                                    data-target="#safecrowCardModal"
                                    @else
                                    data-target="#respondModal"
                                    @endif
                                    data-orderid="{{ $order->id }}">Оставить заявку
                            </button>
                        @endif
                        <a href="#" class="text-danger" data-toggle="modal" data-target="#complaintModal"
                           data-orderid="{{ $order->id }}"><i class="fa fa-exclamation-triangle"></i> Пожаловаться</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="mt-3 mb-3">
    <div class="form__divider">
        <span>Отклики</span>
    </div>
</div>
@foreach($order->activeResponds as $respond)
    <div class="card__subcard">
        <div class="response-body d-flex">
            <div class="response__block d-flex flex-column col-lg-4">
                <div class="part__title text-default mb-4">Исполнитель</div>
                <div class="response__profile d-flex flex-row">
                    <div class="response__image mr-3">
                        <span class="avatar" style="background-image: url('{{ $respond->user->avatar_url }}');"></span>
                    </div>
                    <div class="response__profile-info">
                        <div class="profile-info__name mt-1 mb-1">{{ $respond->user->username }}</div>
                        <div class="rates stars-{{ $respond->user->averageRatings() }}">
                            <i class="fa fa-star star"></i>
                            <i class="fa fa-star star"></i>
                            <i class="fa fa-star star"></i>
                            <i class="fa fa-star star"></i>
                            <i class="fa fa-star star"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="response__block mt-5 mt-lg-0 d-flex flex-column col-lg-8">
                <div class="part__title text-default mb-4">Комментарий</div>
                <p class="response__commentary">
                    {{ $respond->comment }}
                </p>
                {{--<a href="#">Развернуть <i class="fa fa-chevron-down"></i></a>--}}
            </div>
        </div>
        <div class="response__info d-flex justify-content-between mt-6 text-muted">
            <div class="works__amount">
                Всего выполнено работ:
                <span>{{ $respond->user->orders()->whereIn('status', ['done', 'guarantee'])->count() }}</span>
            </div>
            <div class="ratings d-flex">
                <div class="positive mr-4 d-flex text-green align-items-center">
                    <img src="{{asset('img/thumb-up.svg')}}" alt="" class="mr-1">
                    {{ $respond->user->ratings()->where('score', '>=', 0)->count() }}
                </div>
                <div class="negative d-flex text-red align-items-center">
                    <img src="{{asset('img/thumb-down.svg')}}" alt="" class="mr-1">
                    {{ $respond->user->ratings()->where('score', '<=', 0)->count() }}
                </div>
            </div>
            @if(auth()->check() && auth()->user()->id == $order->user->id)
                <div>
                    <form action="{{ route('responds.choose', ['respond_id' => $respond->id]) }}" method="POST">
                        @csrf
                        <button class="btn btn-outline-primary">Выбрать</button>
                    </form>
                </div>
            @endif
        </div>
        <div class="form__divider form__divider_dark mt-4 js-hidden-open cursor-pointer">
            <span>Просмотреть все комментарии ({{ $respond->comments()->count() }})</span>
        </div>
        <div class="hidden__info">
            @foreach($respond->comments as $comment)
                <div class="respond__response d-flex flex-wrap mb-3">
                    <div class="col-md-4">
                        <div class="response__profile d-flex align-items-center">
                            <div class="response__image mr-2">
                                <span class="avatar"
                                      style="background-image: url('{{ $comment->user->avatar_url }}');"></span>
                            </div>
                            <div class="response__profile-info">
                                <div class="profile-info__name">{{ $comment->user->username }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <p class="response__commentary text-muted">
                            {{ $comment->comment }}
                        </p>
                    </div>
                </div>
            @endforeach
            @if(auth()->check() && (auth()->user()->personalOrder($order->id) || auth()->user()->id == $respond->user->id))
                <div class="respond__response mt-5">
                    <form action="{{ route('responds.comment', ['respond' => $respond->id]) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            {{--<label class="response__title mb-2">Оставить комментарий</label>--}}
                            <textarea name="comment" id="respond" placeholder="Введите комментарий" class="form-control"
                                      rows="5"></textarea>
                        </div>
                        <div class="response__send">
                            <div class="attachment">
                                <input id="attachment" multiple type="file" class="attachment__file">
                                <label for="attachment"><i class="fa fa-paperclip"></i> Прикрепить файлы</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Отправить</button>
                        </div>
                    </form>
                </div>
            @endif
        </div>

        <div class="cost absolute">
            {{ $respond->budget }}
        </div>
    </div>
@endforeach