<div class="col-lg-4 order-0 order-lg-1 mb-5 mb-lg-0">
    <div class="card">
        <div class="card-header card-header_border d-flex justify-content-between align-items-center p-5">
            <div class="card__title card__title_small">Детали заказа</div>
        </div>
        <div class="card-body">
            <div class="point">
                <div class="point-icon">
                    <img src="{{asset('img/type.svg')}}" alt="">
                </div>
                <div class="point-description">
                    {{ $order->title }}
                </div>
            </div>
            <div class="point">
                <div class="point-icon">
                    <img src="{{asset('img/calendar.svg')}}" alt="">
                </div>
                <div class="point-description">
                    <a href="#">{{ $order->deadline->format('d-m-Y H:i') }}</a>
                </div>
            </div>
            <div class="point">
                <div class="point-icon">
                    <img src="{{asset('img/pages.svg')}}" alt="">
                </div>
                <div class="point-description">
                    {{ $order->work_amount }} страниц
                </div>
            </div>
            <div class="point">
                <div class="point-icon">
                    <img src="{{asset('img/pencil.svg')}}" alt="">
                </div>
                <div class="point-description">
                    {{ $order->subjects->count() > 0 ? $order->subjects->implode('name', ',') : '' }}
                </div>
            </div>
            <div class="point">
                <div class="point-icon">
                    <img src="{{asset('img/id.svg')}}" alt="">
                </div>
                <div class="point-description">
                    {{ $order->id }}
                </div>
            </div>
            <div class="point">
                <div class="point-icon">
                    <img src="{{asset('img/cost.svg')}}" alt="">
                </div>
                <div class="point-description">
                    {{ $order->budget }}
                </div>
            </div>
        </div>
        @if(auth()->check())
            @if(auth()->user()->can('admin') || auth()->user()->id == $order->user_id && $order->status != \App\Order::STATUS_BANNED)
                <div class="card-footer">
                    <div class="text-red">
                        @can('admin')
                            <a href="{{ route('admin.orders.ban', ['id' => $order->id]) }}" class="text-red">
                                <i class="fa fa-close mr-1"></i>
                                Блокировать заказ
                            </a>
                        @endcan
                        @if(auth()->check() && auth()->user()->id == $order->user_id && $order->status != \App\Order::STATUS_BANNED)
                            <div class="d-flex flex-column">
                                <a href="{{ route('orders.close', ['id' => $order->id]) }}" class="text-red"><i class="fa fa-close"></i> Закрыть заказ</a>
                                @if ($order->status == \App\Order::STATUS_DONE && \Carbon\Carbon::now()->diffInDays($order->done_at) < 20)
                                    <a href="{{ route('orders.renew', ['id' => $order->id]) }}" class="text-primary"><i class="fa fa-retweet"></i> Доработка по гарантии</a>
                                @endif
                            </div>

                        @endif
                    </div>
                </div>
            @endif
        @endif
    </div>

    @can('admin')
        @if($order->complaints->count() > 0)
            @include('admin.parts.order-complaints', ['complaints' => $order->complaints])
        @endif
    @endcan
</div>