@if(auth()->check() && auth()->user()->personalOrder($order->id))
    <div class="mt-3 mb-3">
        <div class="form__divider">
            <span><span class="mb-0 pb-0">Победивший отклик</span></span>
        </div>
    </div>

    <div class="card__subcard">
        <div class="response-body d-flex">
            <div class="response__block d-flex flex-column col-lg-4">
                <div class="part__title text-default mb-4">Исполнитель</div>
                <div class="response__profile d-flex flex-row">
                    <div class="response__image mr-3">
                        <span class="avatar"
                              style="background-image: url('{{ $winnerRespond->user->avatar_url }}');"></span>
                    </div>
                    <div class="response__profile-info">
                        <div class="profile-info__name mt-1 mb-1">{{ $winnerRespond->user->username }}</div>
                        <div class="rates stars-{{ $winnerRespond->user->averageRatings() }}">
                            <i class="fa fa-star star"></i>
                            <i class="fa fa-star star"></i>
                            <i class="fa fa-star star"></i>
                            <i class="fa fa-star star"></i>
                            <i class="fa fa-star star"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="response__block mt-5 mt-lg-0 d-flex flex-column col-lg-8">
                <div class="part__title text-default mb-4">Комментарий</div>
                <p class="response__commentary">
                    {{ $winnerRespond->comment }}
                </p>
                {{--<a href="#">Развернуть <i class="fa fa-chevron-down"></i></a>--}}
            </div>
        </div>
        <div class="response__info d-flex justify-content-between mt-6 text-muted">
            <div class="works__amount">
                Всего выполнено работ:
                <span>{{ $winnerRespond->user->orders()->whereIn('status', ['done', 'guarantee'])->count() }}</span>
            </div>
            <div class="ratings d-flex">
                <div class="positive mr-4 d-flex text-green align-items-center">
                    <img src="{{asset('img/thumb-up.svg')}}" alt="" class="mr-1">
                    {{ $winnerRespond->user->ratings()->where('score', '>=', 0)->count() }}
                </div>
                <div class="negative d-flex text-red align-items-center">
                    <img src="{{asset('img/thumb-down.svg')}}" alt="" class="mr-1">
                    {{ $winnerRespond->user->ratings()->where('score', '<=', 0)->count() }}
                </div>
            </div>
        </div>
    </div>

    <div class="mt-3 mb-3">
        <div class="form__divider">
            <span><span class="mb-0 pb-0">Обсуждение</span></span>
        </div>
    </div>


    @foreach($winnerRespond->comments as $comment)

        <div class="card__subcard">
            <div class="response-body d-flex flex-wrap">
                <div class="date text-muted col-12 mb-3">
                    <i class="fa fa-clock-o"></i>
                    {{ $comment->created_at }}
                </div>
                <div class="response__block col-md-4">
                    <div class="part__title mb-4">
                        @if($winnerRespond->user->id == $comment->user->id)
                            Исполнитель
                        @else
                            Заказчик
                        @endif
                    </div>
                    <div class="response__profile d-flex align-items-center">
                        <div class="response__image mr-2">
                            <span class="avatar"
                                  style="background-image: url('{{ $winnerRespond->user->avatar_url }}');"></span>
                        </div>
                        <div class="response__profile-info">
                            <div class="profile-info__name">{{ $comment->user->username }}</div>
                        </div>
                    </div>
                </div>

                <div class="response__block col-md-8">
                    <div class="part__title mb-4">Комментарий</div>
                    <p class="response__commentary">
                        {{ $comment->comment }}
                    </p>
                    <div class="response__documents">
                        @foreach($comment->files as $file)
                            <a href="{{ \Storage::url($file->path) }}" download>
                                <span class="document__icon"><img src="{{asset('img/file.svg')}}" alt=""></span>
                                <span class="document__name">{{ $file->original_name }}</span>
                            </a>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    @endforeach

    @if(in_array($order->status, \App\Order::COMMENTABLE_STATUSES))
        <div class="card__subcard card__subcard_no-background pt-3">
            <div class="respond__response">
                <form action="{{ route('responds.comment', ['respond' => $winnerRespond->id]) }}"
                      enctype="multipart/form-data" method="POST">
                    {{csrf_field()}}
                    <label class="response__title">Оставить комментарий</label>
                    <textarea name="comment" id="respond" placeholder="Введите комментарий"
                              class="response__textarea form-control {{ $errors->has('comment') ? 'is-invalid' : '' }}">{{ old('comment') }}</textarea>
                    @if($errors->has('comment'))
                        <small class="invalid-feedback">Вы забыли заполнить это поле</small>
                    @endif
                    <div class="response__send">
                        <div class="attachment">
                            <input id="attachment" multiple type="file" name="files[]" class="attachment__file">
                            <label for="attachment"><i class="fa fa-paperclip"></i> Прикрепить файлы</label>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-outline-primary">Отправить</button>

                        </div>
                    </div>
                    @if($winnerRespond->user->id == auth()->user()->id && $order->status == App\Order::STATUS_UNDERWAY)
                        <div class="d-flex flex-wrap justify-content-end custom-control custom-checkbox">
                            {{--<label for="intermediate" class="checkbox-label mr-3">--}}
                            {{--<input type="checkbox" name="intermediate" id="itermediate">--}}
                            {{--Предварительный вариант работы--}}
                            {{--</label>--}}
                            <input type="checkbox" name="final" id="final" class="custom-control-input">
                            <label for="final" class="custom-control-label">
                                Это результат работы. На проверку.
                            </label>
                        </div>
                    @endif
                </form>
            </div>
        </div>
    @endif
    @if($order->status == \App\Order::STATUS_PENDING)
        <div class="card__subcard card__subcard_no-background pt-0">
            <div class="d-flex text-center flex-column mt-5">
                @if($winnerRespond->user->id == auth()->user()->id)
                    <h4 class="mb-4">Работа отмечена, как выполненная. Ожидается проверка соответствия работы
                        требованиям заказчика и подтверждение выполнения.</h4>
                @else

                    <h4 class="mb-4">Работа отмечена, как выполненная. Проверьте соответствие работы требованиям и
                        нажмите "принять" или "отказ"</h4>
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('orders.done', ['id' => $order->id]) }}"
                           class="btn btn-blue mr-3 btn_exact btn-small">Принять</a>
                        <a href="{{ route('orders.decline', ['id' => $order->id]) }}"
                           class="btn btn-blue_outline btn_exact btn-small">Отказ</a>
                    </div>

                @endif
            </div>
        </div>
    @elseif($order->status == \App\Order::STATUS_DONE)
        <div class="card__subcard card__subcard_no-background pt-0">
            <div class="d-flex text-center flex-column mt-5">
                <h4 class="mb-4">Работа успешно завершена!</h4>
            </div>
        </div>
    @elseif($order->status == \App\Order::STATUS_DECLINED)
        <div class="card__subcard card__subcard_no-background pt-0">
            <div class="d-flex text-center flex-column mt-5">
                @if($winnerRespond->user->id == auth()->user()->id)
                    <h4 class="mb-4">Заказчик не принял работу. Вы можете возбудить спор или доработать ее</h4>
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('orders.resume', ['id' => $order->id]) }}"
                           class="btn btn-blue mr-3 btn_exact btn-small">Принять на доработку</a>
                        <a href="{{ route('orders.argue', ['id' => $order->id]) }}"
                           class="btn btn-blue_outline btn_exact btn-small">Спор</a>
                    </div>
                @else
                    <h4 class="mb-4">Ожидается ответ исполнителя на претензию: возврат на доработку или возбуждение
                        спора</h4>
                @endif
            </div>
        </div>
    @elseif($order->status == \App\Order::STATUS_ARGUE)
        <div class="card__subcard card__subcard_no-background pt-0">
            <div class="d-flex text-center flex-column mt-5">
                <h4 class="mb-4">Работа в режиме спора! Ожидается рассмотрение модератором!</h4>
            </div>
        </div>
    @elseif($order->status == \App\Order::STATUS_ACCEPTANCE)
        <div class="card__subcard card__subcard_no-background pt-0">
            <div class="d-flex text-center flex-column mt-5">
                @if($winnerRespond->budget <= 0)
                    @if(auth()->user()->type == \App\User::TYPE_CUSTOMER)
                        <h5 class="mb-4">Ожидается установка исполнителем стоимости выполнения. Текущая стоимость: {{ $winnerRespond->budget }}</h5>
                    @else
                        <h5 class="mb-4">Установите стоимость выполнения.</h5>
                        <div class="d-flex mt-3 justify-content-center">
                            <a href="#" data-toggle="modal" data-target="#setCostModal" class="btn btn-primary mr-2">Установить</a>
                        </div>
                    @endif
                @else
                    <h5 class="mb-4">Идет обсуждение сроков и стоимости работы. По готовности к выполнению заказа,
                        заказчик и исполнитель должны нажать на кнопку ниже. Текущая стоимость: {{ $winnerRespond->budget }}</h5>
                    <div class="d-flex mt-3 justify-content-center">
                        @if (!$order->winner->executant_accepted && auth()->user()->id == $order->winner->user_id)
                                <a href="#" data-toggle="modal" data-target="#acceptRespond" class="btn btn-primary mr-2">Принять</a>
                                <a href="{{ route('responds.reject', ['id' => $winnerRespond]) }}"
                                   class="btn btn-outline-danger">Отказаться</a>
                                <a href="#" data-toggle="modal" data-target="#setCostModal" class="btn btn-primary ml-2">Сменить стоимость</a>
                        @endif
                        @if(auth()->user()->id == $order->user_id && !$order->winner->customer_accepted && $order->winner->executant_accepted)
                            <a href="#" data-toggle="modal" data-target="#acceptRespond" class="btn btn-primary mr-2">Принять</a>
                            <a href="{{ route('responds.reject', ['id' => $winnerRespond]) }}"
                               class="btn btn-outline-danger">Отказаться</a>
                        @endif
                        @if (auth()->user()->id == $order->winner->user_id && $order->winner->executant_accepted)
                            <p>Вы уже согласились с условиями</p>
                        @endif
                        @if (auth()->user()->id == $order->user_id && !$order->winner->executant_accepted)
                            <p>Ожидайте, пока исполнитель подтвердит цену и свое согласие или <a href="{{ route('responds.reject', ['id' => $winnerRespond]) }}">откажитесь</a></p>

                        @endif
                    </div>
                @endif
            </div>
        </div>
    @elseif($order->status == App\Order::STATUS_PAYMENT)
        <div class="card__subcard card__subcard_no-background pt-0">
            <div class="d-flex text-center flex-column mt-5">
                @if(auth()->user()->id == $order->winner->user_id)
                    <h5 class="mb-4">Ожидается оплата</h5>
                @else
                    <h5 class="mb-4">Вы и исполнитель согласились с условиями. Оплатите заказ для начала
                        выполнения.</h5>
                    <div class="d-flex mt-3 justify-content-center flex-column">
                        <div>
                            <a href="{{ $paymentUrl }}"
                               class="btn btn-outline-primary">Оплатить</a>
                        </div>
                        <div>
                            <p class="text-muted">Уже оплатили? Попробуйте <a href="{{ route('orders.updateStatus', ['id' => $order->id]) }}">обновить статус</a></p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    @endif
@endif