<div class="card">
    <div class="card-header border-bottom-0 p-3 p-sm-5">
        <div class="card__title">Лента уведомлений</div>
    </div>
    <div class="card-body card-body_no-top p-3 p-sm-5">
        <div class="d-flex">
            <div class="col-6 notifications__title p-0">
                Текст уведмления
            </div>
            <div class="notifications__title col-6 p-0">
                Вид уведомления
            </div>
        </div>
        <div class="v-bar__wrapper">
            @foreach(auth()->user()->notifications as $notification)
                <div class="notifications">
                <div class="notification d-flex">
                    <div class="col-6 d-flex flex-wrap align-items-center p-0">
                        {{--<div class="notification__profile-image">--}}
                            {{--<span class="avatar" style="background-image: url('{{ asset(auth()->user()->avatar_url) }}');"></span>--}}
                        {{--</div>--}}
                        <div class="notification__profile-name">
                            @if(!$notification->link)
                                {!! $notification->text !!}
                            @else
                                <a href="{{ $notification->link }}">
                                    {!! $notification->text !!}
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="col-6 p-0 d-flex align-items-center">
                        <div class="notification__type notification__type_green">
                            Новый комментарий
                        </div>
                    </div>
                </div>

            </div>
            @endforeach
        </div>
    </div>
</div>