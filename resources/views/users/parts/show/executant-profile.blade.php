<div class="col-lg-7">
    <div class="card">
        <div class="card-header border-0 card-header_small-bottom d-flex flex-wrap justify-content-between align-items-start p-5">
            <div class="card__title">Личные данные</div>
            @owner($user->id)
            <a href="#" class="d-block mt-4 mt-sm-0" @click.prevent="changePassword = !changePassword">Сменить
                пароль</a>
            @endowner
        </div>

        <div class="card-body">
            <div class="row m-0 mb-2">
                <div class="d-flex flex-column justify-content-start align-items-start profile-content col-6">
                    <div class="d-flex align-items-center flex-wrap justify-content-center">
                        <div class="profile__image-block">
                            <span class="avatar avatar-xl" style="background-image: url('{{asset(url($user->avatar_url))}}');"></span>
                        </div>
                        <div class="profile__info-name">
                            {{$user->username}}
                        </div>
                    </div>
                    @owner($user->id)
                        <a href="#" class="btn btn-outline-primary mt-5">Изменить данные</a>
                    @endowner
                </div>
                <form action="" class="col-6">
                    <div class="form__group">
                        <label for="profile-login">Имя пользователя</label>
                        <input type="text" id="profile-login" name="username" class="form-control"
                               value="{{ $user->username }}">
                    </div>
                </form>
            </div>
            <div class="row mr-0 ml-0">
                <div class="col-6">
                    <div class="works__amount">
                        Всего выполнено работ: <span>{{ $user->orders()->whereIn('status', ['done'])->count() }}</span>
                    </div>
                </div>
                <div class="col-6">
                    <div class="ratings">
                        <div class="positive">
                            <img src="{{asset('img/thumb-up.svg')}}" alt="">
                            {{ $user->ratings()->where('score', '>', 2)->count() }}
                        </div>
                        <div class="negative">
                            <img src="{{asset('img/thumb-down.svg')}}" alt="">
                            {{ $user->ratings()->where('score', '<', 3)->count() }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="respond__divider">
                <span>Подробная информация</span>
            </div>

            <div class="profile-info">
                <div class="profile-title">
                    Квалификация
                </div>
                <div class="profile-label">
                    Информация о себе
                </div>
                <div class="profile-information">
                    <p>{{$user->executant_profile->about}}</p>
                </div>
            </div>

            <div class="respond__divider"></div>

            <div class="profile-info d-flex justify-content-between">
                <div class="col p-0">
                    <div class="profile-label">
                        Образование
                    </div>
                    <div class="profile-information">
                        {{$user->executant_profile->education}}
                    </div>
                </div>
                <div class="col p-0">
                    <div class="profile-label">
                        Ученое звание
                    </div>
                    <div class="profile-information">
                        {{$user->executant_profile->degree ? $user->executant_profile->degree->name : '---'}}
                    </div>
                </div>
                <div class="col p-0">
                    <div class="profile-label">
                        Ученая степень
                    </div>
                    <div class="profile-information">
                        {{$user->executant_profile->title ? $user->executant_profile->title->name : '---'}}
                    </div>
                </div>
            </div>

            <div class="respond__divider"></div>

            <div class="profile-info">
                <div class="profile-label mb-3">
                    Типы выполняемых работ
                </div>
                <div class="profile-information">
                    <ul class="list_custom">
                        @foreach($user->executant_profile->work_types as $subject)
                            <li>{{$subject->name}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="respond__divider"></div>

            <div class="profile-info">
                <div class="profile-label mb-3">
                    Специализация
                </div>
                <div class="profile-information">
                    <ul class="list_custom">
                        @foreach($user->executant_profile->specializations as $specialization)
                            <li>{{$specialization->name}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="respond__divider"></div>

            {{--<div class="profile-info">--}}
                {{--<div class="profile-title">--}}
                    {{--Добавленные карты--}}
                {{--</div>--}}
                {{--<div class="profile-label">--}}
                    {{--Номер карты--}}
                {{--</div>--}}
                {{--<div class="profile-information d-flex align-items-center">--}}
                    {{--<div class="card-number mr-2">--}}
                        {{--•••• •••• •••• 0228--}}
                    {{--</div>--}}
                    {{--<div class="card-image_small">--}}
                        {{--<img src="{{asset('img/mastercard.svg')}}" alt="">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>

    <div class="card">
        <div class="card-header border-0">
            <div class="card__title">Статистика заказов</div>
        </div>
        <div class="card-body d-flex pt-0 mt-4">
            <div class="statistics mr-5">
                <div class="statistics__amount statistics__amount_blue">
                    {{ $user->personalOrders()->count() }}
                </div>
                <div class="statistics__description">
                    Заказов всего
                </div>
            </div>
            <div class="statistics mr-5">
                <div class="statistics__amount statistics__amount_green">
                    {{ $user->orders()->personalByUserId($user->id)->whereIn('status', \App\Order::COMMENTABLE_STATUSES)->count() }}
                </div>
                <div class="statistics__description">
                    Сейчас в работе
                </div>
            </div>
        </div>
    </div>
</div>