<div class="col-md-6 offset-md-1">
    <div class="card">
        <div class="card-body d-flex">
            <div class="w-30 d-flex justify-content-center align-items-center">
                <div class="d-flex align-items-center">
                    <div class="profile__image-block">
                        <span class="avatar avatar-xl" style="background-image: url('{{asset(url($user->avatar_url))}}');"></span>
                    </div>
                    <div class="profile__info-name ml-2 h4 pt-1">
                        {{ $user->username }}
                    </div>
                </div>
            </div>
            <div class="w-60 ml-9">
                <div class="card__title">Статистика заказов</div>
                <div class="d-flex mt-4">
                    <div class="statistics mr-5">
                        <div class="statistics__amount statistics__amount_blue">
                            {{ $user->personalOrders()->count() }}
                        </div>
                        <div class="statistics__description">
                            Заказов всего
                        </div>
                    </div>
                    <div class="statistics">
                        <div class="statistics__amount statistics__amount_green">
                            {{ $user->orders()->personalByUserId($user->id)->whereIn('status', \App\Order::COMMENTABLE_STATUSES)->count() }}
                        </div>
                        <div class="statistics__description">
                            Сейчас в работе
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>