<div class="col-lg-8">
    <div class="card">
        <div class="card-header card-header_small-bottom d-flex flex-wrap justify-content-between align-items-start">
            <div class="card__title">Личные данные</div>
            <a href="#" class="d-block mt-4 mt-sm-0" @click.prevent="changePassword = !changePassword">Сменить
                пароль</a>
        </div>

        <div class="card-body">
            <div class="row m-0">
                <div class="d-flex flex-column justify-content-start align-items-start profile-content mr-5 mr-lg-3">
                    <div class="d-flex align-items-center flex-wrap justify-content-center">
                        <div class="profile__image-block">
                            {{-- TODO: Test this on user profile photoes saves--}}
                            <img src="{{asset(url($user->avatar_url))}}" alt="">
                        </div>
                        <div class="profile__info-name">
                            {{$user->username}}
                        </div>
                    </div>
                    <a href="#" class="btn btn-blue_outline btn-small mt-5">Изменить данные</a>
                </div>
                <form action="" class="ml-0 ml-md-5 mt-3 mt-md-0">
                    <div class="form__group">
                        <label for="profile-login">Имя пользователя</label>
                        <input type="text" id="profile-login" name="username" class="input input_profile"
                               value="{{ $user->username }}">
                    </div>
                    <div class="form__group">
                        <label for="profile-login">Имя</label>
                        <input type="text" id="profile-login" name="name" class="input input_profile"
                               value="{{ $user->customer_profile->name }}">
                    </div>

                    <div class="form__group">
                        <label for="profile-login">Фамилия</label>
                        <input type="text" id="profile-login" name="surname" class="input input_profile"
                               value="{{ $user->customer_profile->surname }}">
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>