<div class="card">
    <div class="card-header border-bottom-0 p-5">
        <div class="card__title">Статистика заказов</div>
    </div>
    <div class="card-body d-flex pt-0 flex-wrap">
        <div class="statistics mr-5 mt-2">
            <div class="statistics__amount statistics__amount_blue">
                {{ $user->personalOrders()->count() }}
            </div>
            <div class="statistics__description">
                Заказов всего
            </div>
        </div>
        <div class="statistics mt-2">
            <div class="statistics__amount statistics__amount_green">
                {{ $user->orders()->personalByUserId($user->id)->whereIn('status', ['underway', 'pending', 'argue', 'declined', 'guarantee'])->count() }}
            </div>
            <div class="statistics__description">
                Сейчас в работе
            </div>
        </div>
    </div>
</div>