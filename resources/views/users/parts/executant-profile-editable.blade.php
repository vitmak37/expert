<div class="card">
    <div class="card-header border-0 p-5 card-header_small-bottom d-flex justify-content-between align-items-start">
        <div class="card__title">Личные данные</div>
        <a href="#" class="d-block mt-4 mt-sm-0" data-toggle="modal" data-target="#changePasswordModal">Сменить пароль</a>
        <a href="#" class="d-block mt-4 mt-sm-0" data-toggle="modal" data-target="#setNewImage">Сменить аватар</a>
    </div>
    <a href="{{ action('UserController@show', ['user' => auth()->user()->id]) }}"></a>
    <div class="card-body">

        <div class="d-flex flex-column pb-3">
            <div class="d-flex mb-4">
                <div class="d-flex flex-column align-items-start flex-wrap col-md-6 col-12 p-0">
                    <div class="d-flex align-items-center mb-3 ">
                        <div class="mr-3">
                            <span class="avatar avatar-xxl"
                                  style="background-image: url('{{ asset($user->avatar_url) }}');"></span>
                        </div>
                        <div class="profile__info-name">
                            <h2>{{ $user->username }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__divider"></div>

        <div class="form__title mt-4 mb-3">Квалификация</div>
        <form action="{{ action('\App\Http\Controllers\UserController@update') }}" class="w-100" method="POST">
            {{csrf_field()}}
            <div class="form__group  mb-4">
                <label for="about">Персональная информация</label>
                <textarea name="about" id="about"
                          class="response__textarea form-control"
                          rows="6">{{ $user->executant_profile->about }}</textarea>
            </div>
            <div class="d-flex flex-wrap">
                <div class="col-12 col-md-6 p-0 pr-0 pr-md-2 form__group mb-4">
                    <label for="education">Образование</label>
                    <input type="text" name="education" id="education" class="form-control"
                           value="{{ $user->executant_profile->education }}">
                </div>
                <div class="col-12 col-md-6 p-0 pr-0 pr-md-2 form__group mb-4">
                    <label for="qualification">Квалификация</label>
                    <input type="text" name="qualification" id="qualification" class="form-control"
                           value="{{ $user->executant_profile->qualification }}">
                </div>
                <div class="col-12 col-md-6 p-0 pr-0 pr-md-2 form__group mb-4">
                    <label for="title">Ученое звание</label>
                    <div class="select-input">
                        <select name="academic_title_id" id="title" class="form-control">
                            @foreach($academicTitles as $title)
                                <option value="{{ $title->id }}" {{ $user->executant_profile->academic_title_id == $title->id ? 'selected' : '' }}>{{ $title->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 p-0 pr-0 pr-md-2 form__group mb-4">
                    <label for="degree">Ученая степень</label>
                    <div class="select-input">
                        <select name="academic_degree_id" id="degree" class="form-control">
                            @foreach($academicDegrees as $degree)
                                <option value="{{ $degree->id }}" {{ $user->executant_profile->academic_degree == $degree->id ? 'selected' : '' }}>{{ $degree->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div>
                <label class="mb-1">Типы выполняемых работ</label>
                <div class="form__group d-flex flex-wrap mb-4">

                    @foreach($workTypes as $workType)
                        <div class="d-inline-block custom-control custom-checkbox mr-3">
                            <input type="checkbox" id="work{{ $workType->id }}" name="work[{{ $workType->id }}]"
                                   class="custom-control-input" {{ $user->executant_profile->hasWorkType($workType->id) ? 'checked' : '' }}>
                            <label class="custom-control-label" for="work{{ $workType->id }}">
                                {{ $workType->name }}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div>
                <label class="mb-1">Специализация</label>
                <div class="form__group d-flex flex-wrap  mb-4">

                    @foreach($specializations as $specialization)
                        <div class="d-inline-block custom-control custom-checkbox mr-3">
                            <input type="checkbox" id="specialization{{$specialization->id}}"
                                   name="specialization[{{ $specialization->id }}]" class="custom-control-input"
                                   {{ $user->executant_profile->hasSpecialization($specialization->id) ? 'checked' : '' }} }}>
                            <label class="custom-control-label" for="specialization{{$specialization->id}}">
                                {{ $workType->name }}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="form__group mb-0">
                <button type="submit" class="btn btn-outline-primary">Сохранить</button>
            </div>
        </form>
    </div>

</div>