<div class="col-lg-4 order-0 order-lg-1 mt-3 mt-lg-0">
    <div class="card mb-5 mb-lg-0">
        <div class="card-header card-header_border d-flex justify-content-between align-items-center">
            <div class="card__title card__title_small">Профиль {{ $user->username }}</div>
        </div>
        <div class="card-body">
            <div class="point">
                <a href="#">Личные данные</a>
            </div>
            <div class="point">
                <a href="#">Статистика заказов</a>
            </div>
            <div class="point">
                <a href="#">Уведомления</a>
            </div>
            <div class="point">
                <a href="/logout">Выйти</a>
            </div>
        </div>
    </div>
</div>