<div class="card">
    <div class="card-header border-bottom-0 card-header_small-bottom d-flex flex-wrap justify-content-between align-items-start">
        <div class="card__title">Создание пользовательского профиля</div>
    </div>
    <div class="card-body">
        <div class="row m-0">
            <form action="{{ route('users.profile.set') }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group mb-3">
                    <label for="profile_type">Выберите тип профиля</label>
                    <select name="profile_type" id="profile_type" class="form-control mb-1">
                        <option value="{{ \App\User::TYPE_CUSTOMER }}">Заказчик</option>
                        <option value="{{ \App\User::TYPE_EXECUTANT }}">Исполнитель</option>
                    </select>
                    <small>Работа с заказами будет недоступна до момента создания и заполнения профиля</small>
                </div>
                <button class="btn btn-small btn_exact btn-blue">Создать</button>
            </form>
        </div>
    </div>
</div>