<div class="card">
    <div class="card-header border-bottom-0 p-5 card-header_small-bottom d-flex flex-wrap justify-content-between align-items-start">
        <div class="card__title">Личные данные</div>
        <a href="#" class="d-block mt-4 mt-sm-0" data-toggle="modal" data-target="#changePasswordModal">Сменить пароль</a>
        <a href="#" class="d-block mt-4 mt-sm-0" data-toggle="modal" data-target="#setNewImage">Сменить аватар</a>
    </div>

    <div class="card-body">
        <div class="row m-0">

            <div class="d-flex flex-wrap flex-column justify-content-start align-items-start profile-content col-12 col-md-6">
                <div class="d-flex align-items-center flex-wrap justify-content-center">
                    <div class="profile__image-block mr-2">
                        <span class="avatar avatar-xl"
                              style="background-image: url('{{ asset( $user->avatar_url ) }}');"></span>
                    </div>
                    <div class="profile__info-name">
                        <h3 class="ml-2 pt-3 font-weight-normal">{{ $user->username }}</h3>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>