@extends('layouts.default')

@section('content')

    @include('parts.breadcrumbs', ['title' => 'Мой профиль', 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('profile'), 'name' => 'Мой профиль']
        )
    ])

    <div class="container">
        {{--@if($errors->any())--}}
            {{--<div class="alert alert-danger">--}}
                {{--<ul>--}}
                    {{--@foreach($errors->all() as $error)--}}
                        {{--<li>{{$error}}</li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--@endif--}}

        <div class="row justify-content-center">
            @if($user->type == \App\User::TYPE_CUSTOMER)
                <div class="col-lg-8">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @include('users.parts.customer-profile-editable')

                    @include('users.parts.work-statistics')

                </div>
            @elseif($user->type == \App\User::TYPE_EXECUTANT)
                <div class="col-lg-8">
                    @include('users.parts.executant-profile-editable')

                    @include('users.parts.work-statistics')

                </div>
            @else
                <div class="col-lg-8">
                    @include('users.parts.select-profile-type')
                </div>
            @endif

            {{--@include('users.parts.navigation')--}}
        </div>
    </div>

    @if($user->type)
        @include('modals.password')
        @include('modals.newimage')
        {{--@include('modals.')--}}
    @endif
@endsection