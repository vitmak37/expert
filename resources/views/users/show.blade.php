@extends('layouts.default')

@section('content')
    @include('parts.breadcrumbs', ['title' => 'Профиль пользователя ' . $user->username, 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('users.show', ['id' => $user->id]), 'name' => 'Профиль '. $user->username]
        )
    ])

    <div class="row justify-content-center mr-0 ml-0">
        @can('admin')
            <div class="container">
                <div class="col-12 text-center mb-4">
                    <a href="#" class="text-danger"><i class="fa fa-close mr-1"></i> Заблокировать пользователя</a>
                </div>
            </div>

        @endcan
        @if($user->customer_profile_id != null)
            @include('users.parts.show.customer-profile')
        @elseif($user->executant_profile_id != null)
            @include('users.parts.show.executant-profile')
        @endif
    </div>
@endsection