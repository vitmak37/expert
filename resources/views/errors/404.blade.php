@extends('layouts.default')

@section('content')
    <section class="main__content">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 img_404 d-flex justify-content-end">
                    <img src="{{asset('img/404.svg')}}" alt="">
                </div>
                <div class="col-md-6 d-flex flex-column">
                    <div class="title-404">Упс. Мы не нашли, что искали:(</div>
                    <div class="description-404">Кажется, что-то пошло не так. <br>Попробуйте вернуться на главную страницу и попробовать еще раз.</div>
                    <div>
                        <a href="/" class="btn btn_exact btn-small btn-blue">На главную</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection