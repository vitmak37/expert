@extends('layouts.default')

@section('content')
    @include('parts.breadcrumbs', ['title' => 'Лента уведомлений', 'breadcrumbs' => array(
            ['link' => route('index'), 'name' => 'Главная'],
            ['link' => route('orders'), 'name' => 'Лента уведомлений']
        )
    ])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        @if($orders->count() < 1)
                            <h3 class="p-5">Ни одной активной жалобы не найдено</h3>
                        @else
                            @foreach($orders as $order)
                                @include('admin.parts.order', ['order' => $order])
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            {{--<div class="col-lg-4">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-header card-header_border d-flex justify-content-between align-items-center">--}}
                        {{--<div class="card__title card__title_small">Фильтр жалоб</div>--}}
                        {{--<div class="card__discard"><a href="{{ route('admin.index') }}">сбросить</a></div>--}}
                    {{--</div>--}}
                    {{--<div class="card-body">--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection