<div class="card mt-5">
    <div class="card-header d-flex justify-content-between">
        <div class="card__title card__title_small">
            Список жалоб
        </div>
        <a class="text-green" href="{{ route('admin.markRead', ['id' => $order->id]) }}">
            <i class="fa fa-eye"></i>
        </a>
    </div>
    <div class="card-body">
        @foreach($complaints as $key => $complaint)
            <div class="complaint d-flex border-bottom pb-4 {{ $key == 0 ? '' : 'pt-4' }}">
                @if($complaint->user)
                <div class="submitter">
                    <span class="avatar" style="background-image: url('{{ asset($complaint->user->avatar_url) }}')"></span>
                </div>
                @endif
                <div class="text text-muted ml-3">
                    {{ $complaint->comment }}
                </div>
            </div>
        @endforeach
    </div>
</div>