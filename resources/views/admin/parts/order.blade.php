<div class="card__subcard">
    <div class="d-flex align-items-center">
        <div class="info__element col p-0 text-muted mr-2">
            <i class="fa fa-clock-o mr-1"></i>
            {{ $order->created_at->format('Y-m-d H:i') }}
        </div>
        <div class="info__element col p-0 text-muted mr-2">
            <i class="fa fa-calendar-o mr-1"></i>
            до {{ $order->deadline->format('Y-m-d H:i') }}
        </div>
        <div class="info__element col p-0 text-muted mr-2">
            <span class="fa-icon mr-1">ID</span>
            {{ $order->id }}
        </div>
        <div class="info__element text-red mr-4 p-0">
            <i class="fa fa-legal"></i>
            {{ $order->complaints->count() }}
        </div>
        <div class="cost">
            {{ $order->budget }}
        </div>
    </div>

    <div class="order__info">
        <div class="order__title">
            <a href="{{ route('orders.show', ['id' => $order->id]) }}">{{ $order->title }}</a>
        </div>
        <div class="order__categories order__categories_long">
            @foreach($order->subjects as $subject)
                <span>{{ $subject->name }}</span>
            @endforeach
        </div>
    </div>

    <div class="form__divider form__divider_dark mt-4 cursor-pointer js-hidden-open">
        <span>Подробнее <i class="fa fa-chevron-down"></i></span>
    </div>

    <div class="hidden__info">
        <div class="subcard__info mt-3">
            <div class="col-lg-4 d-flex flex-column p-0">
                <div class="text-default mb-4">
                    Заказчик
                </div>
                <div class="d-flex align-items-center">
                    <div class="response__image mr-3">
                        <span class="avatar" style="background-image: url('{{ $order->user->avatar_url }}')"></span>
                    </div>
                    <div class="response__profile-info">
                        <div class="profile-info__name">{{ $order->user->username }}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 d-flex flex-column p-0">
                <div class="text-default mb-4">
                    Информация о заказе
                </div>
                <div>
                    <div class="response__commentary">
                        <p>{{ $order->description }}</p>
                    </div>
                    <div class="subcard__info mt-3">
                        <div class="d-flex mr-3">
                            <div class="point-icon mr-2">
                                <img src="/img/pages.svg" alt="">
                            </div>
                            <div class="point-description">
                                {{ $order->work_amount }} страниц
                            </div>
                        </div>
                        <div class="response__documents">
                            @foreach($order->files as $file)
                                <div>
                                    <span class="document__icon">
                                        <img src="/img/file.svg" alt=""></span>
                                    <a download href="{{ Storage::url($file->path) }}" class="document__name">{{ $file->original_name }}</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>