@extends('layouts.base')

@section('title', 'Эксперт24 - Биржа студенческих работ')

@section('styles')
    <style>
        body{
            background-color: #fff;
        }
    </style>
@endsection

@section('body')
    @include('parts.header')
    @include('index.parts.promo')
    @include('index.parts.calculator')
    @include('index.parts.pros')
    @include('parts.footer')

    @if(!auth()->check())
        @include('modals.register')
        @include('modals.authorization')
    @endif
    @if(session()->has('successModal'))
        @include('modals.success', ['successTitle' => session('successModal')['title'], 'successMessage' => session('successModal')['message']])
    @endif

    @if(session()->has('errorModal'))
        @include('modals.error', ['errorTitle' => session('errorModal')['title'], 'errorMessage' => session('errorModal')['message']])
    @endif

@endsection