<?php

return [
    'no_profile' => 'Profile type error. Please contact administration for further information',
    'success' => 'Successful!'
];