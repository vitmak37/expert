<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Вы должны подтвердить :attribute',
    'active_url'           => 'Поле :attribute не является действительной ссылкой',
    'after'                => 'Поле :attribute должно быть позже :date.',
    'after_or_equal'       => 'Поле :attribute должно быть после или в день :date.',
    'alpha'                => 'Поле :attribute должно содержать только буквы.',
    'alpha_dash'           => 'Поле :attribute может содержать только буквы, цифры и подчеркивания.',
    'alpha_num'            => 'Поле :attribute может содержать только буквы и цифры.',
    'array'                => 'Поле :attribute должно быть массивом.',
    'before'               => 'Поле :attribute должно быть меньше :date.',
    'before_or_equal'      => 'Поле :attribute должно быть меньше или равно :date.',
    'between'              => [
        'numeric' => 'Поле :attribute должно быть от :min до :max.',
        'file'    => 'Поле :attribute должно быть от :min до :max килобайт.',
        'string'  => 'Поле :attribute должно содержать от :min до :max знаков.',
        'array'   => 'Поле :attribute должно содержать от :min до :max элементов.',
    ],
    'boolean'              => 'Поле :attribute должно быть истиной или ложью.',
    'confirmed'            => 'Поле :attribute не совпадает с подтверждением.',
    'date'                 => 'Поле :attribute не правильная дата.',
    'date_format'          => 'Поле :attribute не совпадает с форматом :format.',
    'different'            => 'Поля :attribute и :other должны быть разными.',
    'digits'               => 'Поле :attribute должно иметь :digits знаков после запятой.',
    'digits_between'       => 'Поле :attribute должно иметь от :min до :max знаков после запятой.',
    'dimensions'           => 'Поле :attribute имеет неправильное разрешение.',
    'distinct'             => 'Поле :attribute имеет не уникальные значения.',
    'email'                => 'Поле :attribute должно быть действующим адресом электронной почты.',
    'exists'               => 'Необходимое поле :attribute не существует.',
    'file'                 => 'Поле :attribute должно быть файлом.',
    'filled'               => 'Поле :attribute должно содержать какое-то значение.',
    'gt'                   => [
        'numeric' => 'Поле :attribute должно быть больше чем :value.',
        'file'    => 'Поле :attribute должно быть больше чем :value килобайт.',
        'string'  => 'Поле :attribute должно быть больше чем :value знаков.',
        'array'   => 'Поле :attribute должно содержать :value элементов.',
    ],
    'gte'                  => [
        'numeric' => 'Поле :attribute должно быть больше или равно :value.',
        'file'    => 'Поле :attribute должно быть больше или равно :value kilobytes.',
        'string'  => 'Поле :attribute должно быть больше или равно :value characters.',
        'array'   => 'Поле :attribute должно содержать :value элементов или более.',
    ],
    'image'                => 'Поле :attribute должно быть изображением.',
    'in'                   => 'Поле :attribute не существует в базе данных.',
    'in_array'             => 'Поле :attribute не существует в :other.',
    'integer'              => 'Поле :attribute должно быть целым числом.',
    'ip'                   => 'Поле :attribute должно быть корректным IP адресом.',
    'ipv4'                 => 'Поле :attribute должно быть корректным IPv4 адресом.',
    'ipv6'                 => 'Поле :attribute должно быть корректным IPv6 адресом.',
    'json'                 => 'Поле :attribute должно быть корректным JSON.',
    'lt'                   => [
        'numeric' => 'Поле :attribute должно быть меньше чем :value.',
        'file'    => 'Поле :attribute должно быть меньше чем :value Килобайт.',
        'string'  => 'Поле :attribute должно быть меньше чем :value знаков.',
        'array'   => 'Поле :attribute должно иметь меньше чем :value элементов.',
    ],
    'lte'                  => [
        'numeric' => 'Поле :attribute должно быть меньше или равно :value.',
        'file'    => 'Поле :attribute должно быть меньше или равно :value килобайт.',
        'string'  => 'Поле :attribute должно быть меньше или равно :value знаков.',
        'array'   => 'Поле :attribute не должно содержать больше чем :value элементов.',
    ],
    'max'                  => [
        'numeric' => 'Поле :attribute не может быть больше чем :max.',
        'file'    => 'Поле :attribute не может быть больше чем :max килобайт.',
        'string'  => 'Поле :attribute не может быть больше чем :max знаков.',
        'array'   => 'Поле :attribute не может содержать больше чем :max элементов.',
    ],
    'mimes'                => 'Поле :attribute должно быть файлом типа: :values.',
    'mimetypes'            => 'Поле :attribute должно быть файлом типа: :values.',
    'min'                  => [
        'numeric' => 'Поле :attribute должно быть не меньше :min.',
        'file'    => 'Поле :attribute должно быть не меньше :min килобайт.',
        'string'  => 'Поле :attribute должно быть не меньше :min знаков.',
        'array'   => 'Поле :attribute должно иметь не меньше :min элементов.',
    ],
    'not_in'               => 'Поле :attribute некорректно.',
    'not_regex'            => 'Поле :attribute имеет некорректный формат.',
    'numeric'              => 'Поле :attribute должно быть числом.',
    'present'              => 'Поле :attribute должно присутствовать.',
    'regex'                => 'Поле :attribute имеет некорректный формат.',
    'required'             => 'Поле :attribute является обязательным.',
    'required_if'          => 'Поле :attribute является обязательным когда :other является :value.',
    'required_unless'      => 'Поле :attribute является обязательным пока :other входит в :values.',
    'required_with'        => 'Поле :attribute является обязательным когда добавлено :values.',
    'required_with_all'    => 'Поле :attribute является обязательным когда добавлены :values.',
    'required_without'     => 'Поле :attribute является обязательным пока не добавлено :values .',
    'required_without_all' => 'Поле :attribute является обязательным пока не добавлены :values.',
    'same'                 => 'Поле :attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => 'Поле :attribute должно быть :size.',
        'file'    => 'Поле :attribute должно быть :size килобайт.',
        'string'  => 'Поле :attribute должно быть :size знаков.',
        'array'   => 'Поле :attribute должно содержать :size элементов.',
    ],
    'string'               => 'Поле :attribute должно быть строкой.',
    'timezone'             => 'Поле :attribute должно быть временной зоной.',
    'unique'               => 'Поле :attribute уже используется.',
    'uploaded'             => 'Поле :attribute не загружено.',
    'url'                  => 'Поле :attribute имеет некорректный формат url.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
