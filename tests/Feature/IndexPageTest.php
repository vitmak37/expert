<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexPageTest extends TestCase
{
    public function testIndexPageIsAvailable()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
