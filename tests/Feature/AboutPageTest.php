<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AboutPageTest extends TestCase
{
    public function testAboutPageIsAvailable()
    {
        $response = $this->get(route('about'));
        $response->assertStatus(200);
    }
}
