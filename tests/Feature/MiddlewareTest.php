<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MiddlewareTest extends TestCase
{
    public function setUp(){
        parent::setUp();
        \Route::middleware('contacts')->any('/_test/contacts', function (){
            return 'OK';
        });
    }

    public function testShowsWarningsOnAllTriggers(){
        $triggers = array(
            'айди' => 1,
            'aidi' => 1,
            'айдиали' => 1,
            'пишите мне в вк 123' => 1,
            'идиома' => 0,
            'вконтакте' => 1,
            'vkontakte' => 1,
            'инстаче' => 1,
            'instagram' => 1,
            'insta' => 1,
            'facebook' => 1,
            'фэйсбук' => 1,
            'телеге' => 1,
            'твит' => 1,
            'вибер' => 1,
            'воцапе' => 1,
            'аська' => 1,
            'скуп' => 1,
            '89118888888' => 1,
            '+7 (911) - 888 - 88 - 88' => 1
        );

        foreach($triggers as $t=>$v){
            $response = $this->post('/_test/contacts', ['content' => $t]);

            if($v == 1) {
                $response->assertSee('FAILED');
            }
            else
                $response->assertSee('OK');
        }
    }
}
