<?php

namespace Windstep\Safecrow\Contracts;

interface SafecrowMethodsContract
{
    /**
     * Генерирует HMAC пароль для SafeCrow
     *
     * @param string $endpoint Конечная точка запроса ('/users?email=qwerty@qwerty.qw')
     * @param string $qweryType Тип запроса ('GET', 'POST', 'DELETE", 'PUT')
     * @return string HMAC подпись тела запроса ключом
     */
    public function generateHmacSign(string $endpoint, string $qweryType="GET") : string;

    /**
     * Отправляет запрос на сервера SafeCrow и возвращает ответ оттуда
     *
     * @return mixed
     */
    public function request(string $method, string $endpoint, array $body = null) : \Psr\Http\Message\StreamInterface;

    /**
     * Пытаемся зарегистрировать пользователя
     *
     * @return mixed
     */
    public function registerUser();

    public function showUsers();

    public function showUser();

    public function updateUser();

    public function calculateFee(array $attributes);

    public function createOrder(array $attributes);

    /**
     * Создает заказ с оплатой за доставку
     *
     * @return mixed
     */
    public function createAcquiringOrder();

    public function viewOrders();

    /**
     * Отменяет заказ до его оплаты
     *
     * @return mixed
     */
    public function annulOrder(int $id, array $attributes);

    public function pay();

    public function getAttachCardLink();

    public function viewAttachedCards(int $id);

    public function attachCardToOrder(int $id,int $order_id, array $attributes);

    /**
     * Отменяет заказ после оплаты
     *
     * @return mixed
     */
    public function cancelOrder(int $id, array $attributes);

    /**
     * Закрывает сделку, как успешную
     *
     * @return mixed
     */
    public function closeOrder(int $id);

    /**
     * Претензия к сделке
     *
     * @return mixed
     */
    public function complain();

    /**
     * Прикрепляет вложение к сделке
     *
     * @return mixed
     */
    public function attach();

    /**
     * Позволяет просмотреть вложения конкретной сделки
     *
     * @return mixed
     */
    public function viewAttaches();

    /**
     * Позволяет настроить URL для коллбэка
     *
     * @return mixed
     */
    public function setup();

    public function reauthorizePay();
}