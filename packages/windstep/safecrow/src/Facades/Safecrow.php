<?php

namespace Windstep\Safecrow\Facades;

use Illuminate\Support\Facades\Facade;

class Safecrow extends Facade
{
    public static function getFacadeAccessor(){
        return 'safecrow';
    }
}