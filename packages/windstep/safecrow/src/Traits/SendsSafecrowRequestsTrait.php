<?php

namespace Windstep\Safecrow\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mockery\Exception;
use Windstep\Safecrow\Exceptions\SafecrowAuthorizationException;
use Windstep\Safecrow\Models\SafecrowExecutionException;

trait SendsSafecrowRequestsTrait
{
    /**
     * Внутренний метод, позволяющий сгенерировать рабочую подпись для запросов Safecrow.
     * Описание работы можно найти в документации.
     *
     * @param string $endpoint Представляет собой строку вида '/users?email=email@mail.mail'
     * @param string $method Представляет собой строку вида 'POST' 'GET' и других
     * @return string HMAC подпись для запроса
     */
    public static function generateHmacSign(string $endpoint, string $method, array $attributes = null)
    {
        if ($method == 'GET') {
            $data = self::generateGetHmacData($endpoint);
        } else if ($method == 'POST') {
            $data = self::generatePostHmacData($endpoint, $attributes);
        }

        return hash_hmac('SHA256', $data, config('safecrow.api_secret'));
    }

    /**
     * Метод позволяет отправить запрос на сервера SafeCrow
     * Возвращает ответ от этих серверов.
     *
     * @param string $method
     * @param string $endpoint
     * @param array $attributes
     * @return \Psr\Http\Message\StreamInterface
     * @throws SafecrowAuthorizationException
     */
//    public static function request(string $method, string $endpoint, array $attributes = [])
//    {
//        $client = new Client(['base_uri' => config('safecrow.api_url')]);
//        $hmac = self::generateHmacSign($endpoint, $method, $attributes);
//        try{
//            $response = $client->request($method, config('safecrow.api_prefix') . $endpoint,
//                [
//                    "auth" => [config('safecrow.api_key'), $hmac],
//                    RequestOptions::JSON => $attributes,
//                    "defaults" => ["verify" => false],
//                    "headers" => [
//                        "Content-Type" => "application/json"
//                    ]
//                ]);
//            return json_decode($response->getBody(), true);
//        }catch(GuzzleException $e) {
//            if ($e->getCode() == 404)
//                throw new ModelNotFoundException();
//            if ($e->getCode() == 401)
//                throw new SafecrowAuthorizationException($hmac, $endpoint);
//            //report($e);
//            dd($e);
//        }
//    }

    protected static function request(string $method, string $endpoint, array $attributes = [])
    {
        $hmac = self::generateHmacSign($endpoint, $method, $attributes);
        $api_key = config('safecrow.api_key');
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL,config('safecrow.api_url') . config('safecrow.api_prefix') . $endpoint);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        \curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        \curl_setopt($ch, CURLOPT_USERPWD, "{$api_key}:{$hmac}");
        \curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        if ($method == 'POST') {
            \curl_setopt($ch, CURLOPT_POST, 1);
            \curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($attributes));
        }
        $body = \curl_exec($ch);

        if ($body == false) {
            throw new SafecrowExecutionException(curl_error($ch), curl_errno($ch));
        }

        \Log::info($body);

        if (\curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 401) {
            throw new SafecrowAuthorizationException($hmac, $endpoint);
        }

        if (\curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 500) {
            throw new Exception('Safecrow unavailable');
        }

        \curl_close($ch);
        return json_decode($body);

    }

    protected static function generateGetHmacData(string $endpoint)
    {
        return config('safecrow.api_key') . 'GET' . config('safecrow.api_prefix') . $endpoint;
    }

    protected static function generatePostHmacData(string $endpoint, array $attributes)
    {
        return config('safecrow.api_key') . 'POST' . config('safecrow.api_prefix') . $endpoint . json_encode($attributes);
    }
}