<?php

namespace Windstep\Safecrow\Providers;

use Illuminate\Support\ServiceProvider;
use Windstep\Safecrow\Contracts\SafecrowMethodsContract as SafecrowContract;
use Windstep\Safecrow\SafecrowApi;

class SafeCrowServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('safecrow', function(){
            return new SafecrowApi();
        });
    }
}
