<?php

namespace Windstep\Safecrow\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Jenssegers\Model\Model;
use Windstep\Safecrow\Facades\Safecrow;

class SafecrowUser extends Model
{

    protected $casts = [
        'id' => "integer",
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'registered_at' => 'datetime'
    ];

    protected $fillable = ['id', 'name', 'phone', 'email', 'registered_at'];
    protected $guarded = ['id'];
    //protected $hidden = ['id', 'registered_at'];

    public static function findByEmail($email): self
    {
        $data = Safecrow::findUserByEmail($email);
        return new self($data);
    }

    /**
     * @return Collection
     */
    public static function all(): Collection
    {
        $data = Safecrow::getUsers();
        $d1 = collect();
        foreach($data as $d)
            $d1->push(new self($d));
        return $d1;
    }

    public function save()
    {
        if(!$this->id)
            return self::create($this->getArrayableAttributes());
        return $this->update();
    }

    public function update(): self
    {
        return new self(Safecrow::editUser($this->id, $this->getArrayableAttributes()));
    }

    public static function create(array $attributes): self
    {
        return new self(Safecrow::createUser($attributes));
    }

    public static function find($id)
    {
        try{
            $arr = Safecrow::findUser($id);
        }catch(ModelNotFoundException $e){
            $arr = null;
        }

        return $arr == null ? null : new self($arr);
    }

    public static function findOrFail($id): self
    {
        $arr = Safecrow::findUser($id);
        return new self($arr);
    }

    public function orders(){
        return Safecrow::viewOrdersByUser($this->id);
    }

    public function attachCard($redirect_url){
        return Safecrow::attachCardToUser($this->id, array('redirect_url' => $redirect_url));
    }

    public function viewCards(){
        return Safecrow::viewAttachedCards($this->id);
    }


}