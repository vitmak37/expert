<?php

namespace Windstep\Safecrow\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Jenssegers\Model\Model;
use Windstep\Safecrow\Facades\Safecrow;

class SafecrowOrder extends Model
{

    protected $casts = [
        'id' => "integer",
        'consumer_id' => 'integer',
        "supplier_id" => 'integer',
        "price" => 'integer',
        "consumer_service_cost" => 'integer',
        "supplier_service_cost" => 'integer',
        "status" => 'string',
        "description" => 'string',
        "supplier_payout_method_id" => 'integer',
        "consumer_payout_method_id" => 'integer',
        "supplier_payout_method_type" => 'string',
        "consumer_payout_method_type" => 'integer',
        "created_at" => 'datetime',
        "updated_at" => 'datetime',
        "extra" => "array"
    ];

    protected $fillable = ['id', 'consumer_id', "supplier_id", "description", "price", "consumer_service_cost", "supplier_service_cost", "status", "supplier_payout_method_id", "consumer_payout_method_type", "created_at", "updated_at", "extra"];
    protected $guarded = ['id', "consumer_service_cost", "supplier_service_cost", "status", "supplier_payout_method_id", "consumer_payout_method_type", "created_at", "updated_at", "extra"];

    public function getPriceAttribute(){
        return $this->attributes['price'] / 100;
    }

    public function __construct($attributes)
    {
        if($attributes == null)
            return null;
        //if(typeOf($attributes) == 'array')
        parent::__construct($attributes);
    }

    /**
     * В массиве следующие параметры
     * $consumer_id,
     * $supplier_id,
     * $price (в копейках)
     * $description
     * $service_cost_payer enum('50/50', 'consumer', 'supplier')
     *
     * @param array $attributes
     * @return SafecrowOrder
     */
    public static function create(array $attributes){
        return new self(Safecrow::createOrder($attributes));
    }

    public static function find($id){
        try{
            $arr = Safecrow::findOrder($id);
        }catch(ModelNotFoundException $e){
            $arr = null;
        }

        return $arr == null ? null : new self($arr);
    }

    public static function all(){
        return self::parseCollection(Safecrow::getOrders());
    }

    public function findByUserId(int $user_id){
        return self::parseCollection(Safecrow::viewOrdersByUser($user_id));
    }

    public function pay(string $redirect_url){
        $attributes = ['redirect_url'=> $redirect_url];
        return Safecrow::payOrder($this->id, $attributes);
    }

    public function annul(string $reason){
        $attributes = ['reason' => $reason];
        return new self(Safecrow::annulOrder($this->id, $attributes));
    }

    public function close(){
        return new self(Safecrow::closeOrder($this->id));
    }

    public function cancel($reason){
        $attributes = ['reason' => $reason];
        return new self(Safecrow::cancelOrder($this->id, $attributes));
    }

    public function escalate(string $reason){
        $attributes = ['reason' => $reason];
        return new self(Safecrow::escalate($this->id, $attributes));
    }

    private static function parseCollection($data){
        $d1 = collect();
        foreach($data as $d)
            $d1->push(new self($d));
        return $d1;
    }
}