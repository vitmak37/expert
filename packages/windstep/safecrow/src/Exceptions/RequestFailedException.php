<?php

namespace Windstep\Safecrow\Exceptions;

use GuzzleHttp\Exception\RequestException;

class RequestFailedException extends RequestException
{
    public static function create(){
        return new self('Sorry, something went wrong');
    }
}