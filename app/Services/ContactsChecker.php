<?php

namespace App\Services;

class ContactsChecker{

    private static $regular = '/(айди)|(aidi)|((\s|^)ид\s)|((\s|^)вк(а)?\s)|((\s|^)в\sвк(а)\s)|((\s|^)v\s?v[kcкс](a|а)?)|(v[kc][аa]\s)|((в\s?)?контакт(е)?)|(v\s?[kcск][оo]nt[аa](k|c)te?)|([cсkк][oо]nt[аa][cсkк]te?)|(инста?(грам)?)|(inst.?(gram)?)|(ф(э|й)й[сс](бук)?)|((\s|^)((фб)|(fb))\s)|(f[aа][cс][eе]b[oо]{2}[kcкс])|(teleg(ram)?)|(те(л|р)ег.(ам)?)|(тви(тт?ер.?)?)|(однокласс?ник)|(twitt?(er)?)|(viber)|(в[аи]й?б[еи]р)|(в[оа]т?с?ц?ап)|(wh?at[sc]app?)|(ск[ау]й?п)|(s[kc][yi]p)|(icq)|(ай?си?ь?к)|((\s|^)\+?\d[\s(\-]?\s?[(\-]?\s?\d{3}\s?[)\-\s]?\s?[\-\s]?\s?\d{3}\s?[\-\s]?\s?\d{2}\s?[\-\s]?\s?\d{2})|(@\w*)|(http)/iu';

    /**
     * @param $text
     * @return false|int
     */
    public static function check($text){
        return preg_match(self::$regular, $text);
    }

}