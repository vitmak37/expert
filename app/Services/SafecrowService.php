<?php

namespace App\Services;

use App\Order;
use InvalidArgumentException;
use Windstep\Safecrow\Facades\Safecrow;
use App\Exceptions\TooManyUsersException;
use App\Exceptions\SafecrowResponseException;

class SafecrowService
{
    protected $userRequestResult;

    public function checkUserExists(string $email)
    {
        $this->userRequestResult = $this->userRequestResult ?? Safecrow::findUserByEmail($email);

        if ($this->userRequestResult && gettype($this->userRequestResult) == 'array' && !empty($this->userRequestResult)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $email
     * @return string|null
     * @throws TooManyUsersException
     */
    public function getUserId(string $email)
    {
        if (self::checkUserExists($email)) {
            if (count($this->userRequestResult) > 1) {
                throw new TooManyUsersException();
            }

            return $this->userRequestResult[0]->id;
        }
        return null;
    }

    public function getAllUsers()
    {
        return Safecrow::getUsers();
    }

    /**
     * @param array $attributes
     * @return null|int
     * @throws InvalidArgumentException
     * @throws TooManyUsersException;
     */
    public function getOrCreateUser(array $attributes)
    {
        if (!$this->checkAttributes($attributes)) {
            throw new InvalidArgumentException('Need to specify email, phone and name');
        }
        try {
            $userId = $this->getUserId($attributes['email']);

            if (!$userId) {
                $userId = $this->createUser($attributes);
            }

            return $userId;
        } catch (TooManyUsersException $e) {
            throw new TooManyUsersException('Too many users with email ' . $attributes['email']);
        }
    }

    public function checkAttributes(array $attributes)
    {
        if ($attributes && isset($attributes['email']) && isset($attributes['phone']) && isset($attributes['name'])) {
            return true;
        }

        return false;
    }

    /**
     * @param array $attributes
     * @throws InvalidArgumentException
     */
    public function createUser(array $attributes)
    {
        //TODO сделать этот метод с проверкой на "что-то пошло не так".
        if ($this->checkAttributes($attributes)) {
            $response = Safecrow::createUser($attributes);
            if (isset($response->errors)) {
                throw new SafecrowResponseException($response);
            }
            if (isset($response->id)) {
                return $response->id;
            }else {
                \Log::info((array) $response);
                throw new InvalidArgumentException();
            }
        } else {
            throw new InvalidArgumentException();
        }
    }

    public function getCardUrl($userId, array $attributes)
    {
        $link = Safecrow::getAttachCardLink($userId, $attributes);
        return $link->redirect_url;
    }

    public function hasAssignedCard($userId)
    {
        $cards = Safecrow::viewAttachedCards($userId);
        if (count($cards) > 0) {
            return true;
        }

        return false;
    }

    public function attachedCards($userId)
    {
        return Safecrow::viewAttachedCards($userId);
    }

    public function assignCardsToOrder($orderId, $executantCard, $scUserId)
    {
        return Safecrow::attachCardToOrder($scUserId, $orderId, ["supplier_payout_card_id" => $executantCard]);
    }

    public function createOrder($orderId)
    {
        $order = Order::findOrFail($orderId);
        $attributes = [
            'consumer_id' => $order->user->sc_user_id,
            'supplier_id' => $order->winner->user->sc_user_id,
            'price' => intval($order->winner->budget) * 100,
            'description' => $order->title,
            'service_cost_payer' => 'supplier'
        ];

        $response = Safecrow::createOrder($attributes);
        return $response->id;
    }

    public function getPaymentUrl($scOrderId, $orderId)
    {
        $response = Safecrow::payOrder($scOrderId, ['redirect_url' => route('orders.show', ['id' => $orderId])]);
        return $response->redirect_url;
    }

    public function getOrders()
    {
        return Safecrow::viewOrdersByUser(auth()->user()->sc_user_id);
    }

    public function closeOrder($orderId, string $reason)
    {
        $response = Safecrow::findOrder($orderId);
        if ($response->status != 'paid') {
            $sc = Safecrow::annulOrder($orderId, ['reason' => $reason]);
        } else {
            $sc = Safecrow::cancelOrder($orderId, ['reason' => $reason]);
        }
        return true;
    }
}