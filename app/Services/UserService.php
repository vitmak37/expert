<?php

namespace App\Services;

use App\User;
use Illuminate\Validation\ValidationException;
use Validator;

class UserService
{
    /**
     * @param User $user
     * @param array $parameters
     * @return bool
     * @throws ValidationException
     * @throws \Exception
     */
    public static function update(User $user, array $parameters) : bool
    {
        $validator = Validator::make($parameters, $user->getValidationParameters());
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $parameters = collect($parameters);
        // $user->update($parameters->only(['username'])->toArray());
        if ($user->type == User::TYPE_CUSTOMER) {
            // Pass due to bad customer profile lack of fields
        } else if ($user->type == User::TYPE_EXECUTANT) {
            $user->executant_profile()->update($parameters->only(['qualification', 'about', 'education', 'academic_title_id', 'academic_degree_id'])->toArray());
            $user->executant_profile->work_types()->sync($parameters->has('work') ? array_keys($parameters->get('work')) : []);
            $user->executant_profile->specializations()->sync($parameters->has('specialization') ? array_keys($parameters->get('specialization')) : []);
        }

        return true;
    }
}