<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Specialization extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function executants(){
        return $this->belongsToMany(ExecutantProfile::class, 'executant_profile_has_specializations', 'specializations_id', 'executant_profile_id');
    }
}
