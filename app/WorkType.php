<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkType extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $guarded = [];


    public function executants(){
        return $this->belongsToMany(ExecutantProfile::class, 'executant_profile_has_work_types', 'work_types_id', 'executant_profile_id');
    }

    public function orders(){
        return $this->belongsToMany(Order::class, 'work_types_orders', 'work_types_id', 'order_id');
    }
}
