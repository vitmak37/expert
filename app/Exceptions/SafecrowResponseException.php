<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 23.10.2018
 * Time: 15:14
 */

namespace App\Exceptions;


use Throwable;

class SafecrowResponseException extends \Exception
{
    protected $response;

    public function __construct($response = null, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->response = $response;
        \Log::info((array) $response);
        parent::__construct($message, $code, $previous);
    }

    public function getResponse()
    {
        return $this->response;
    }
}