<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function executant(){
        return $this->belongsTo(User::class, 'executant_id');
    }
}
