<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExecutantProfile extends Model
{
    protected $fillable = ['name', 'surname', 'qualification', 'about', 'education', 'academic_title_id', 'academic_degree_id'];

    public function degree(){
        return $this->belongsTo(AcademicDegree::class, 'academic_degree_id');
    }

    public function title(){
        return $this->belongsTo(AcademicTitle::class, 'academic_title_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'executant_profile_id');
    }

    public function files(){
        return $this->hasOne(File::class, 'files_id');
    }

    public function work_types(){
        return $this->belongsToMany(WorkType::class, 'executant_profile_has_work_types', 'executant_profile_id','work_types_id');
    }

    public function specializations(){
        return $this->belongsToMany(Specialization::class, 'executant_profile_has_specialization','executant_profile_id', 'specialization_id');
    }

    public function hasWorkType($id)
    {
        $collection = $this->work_types;
        return $collection->where('id', $id)->count() > 0;
    }

    public function hasSpecialization($id)
    {
        $collection = $this->specializations();
        return $collection->where('id', $id)->count() > 0;
    }
}
