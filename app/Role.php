<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $table = 'site_roles';

    protected $fillable = [
        'name',
    ];

    public $timestamps = true;

    public function users(){
        return $this->hasMany(User::class, 'roles_id');
    }
}
