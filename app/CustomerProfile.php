<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerProfile extends Model
{
    protected $fillable = ['name', 'surname'];

    public function user(){
        return $this->hasOne(User::class, 'customer_profile_id');
    }
}
