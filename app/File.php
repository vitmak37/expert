<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $guarded = [];

    public function executant_profile(){
        return $this->hasOne(ExecutantProfile::class, 'files_id');
    }

    /**
     * Можно получить пользователя, для которого этот аватар, в случае, если это аватар.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user(){
        return $this->hasOne(User::class, 'avatar_file_id');
    }

    public function orders(){
        return $this->belongsToMany(Order::class, 'order_has_files', 'file_id', 'order_id');
    }

    public function comments(){
        return $this->belongsToMany(Comment::class, 'comments_has_files', 'file_id', 'comment_id');
    }
}
