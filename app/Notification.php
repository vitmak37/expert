<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $table = 'site_notifications';

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function send($to, $message, $link = null)
    {
        self::create(['user_id' => $to, 'text' => $message, 'link' => $link]);
    }
}
