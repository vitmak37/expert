<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respond extends Model
{
    protected $guarded = [];

    public function comments(){
        return $this->hasMany(Comment::class, 'responds_id');
    }

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getBudgetAttribute($budget)
    {
        if ($budget == 0) {
            return 'По договоренности';
        } else {
            return $budget . ' руб.';
        }
    }

    public function scopeIsVisible()
    {
        return $this->whereIn('status', ['pending', 'accepted', 'started', 'prepaid', 'finished', 'failed']);
    }
}
