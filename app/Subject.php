<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $with = ['knowledgeArea'];

    public function orders(){
        return $this->belongsToMany(Order::class, 'subject_orders', 'subjects_id', 'orders_id');
    }

    public function knowledgeArea()
    {
        return $this->belongsTo(KnowledgeArea::class);
    }
}
