<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{

    protected $fillable = ['user_id', 'order_id', 'comment'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }

    public static function markReadById($orderId) {
        return self::where('order_id', $orderId)->update(['is_read' => 1]);
    }
}
