<?php

namespace App\Http\Middleware;

use App\Services\ContactsChecker;
use Closure;

class ContactInfoCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->except(['phone', 'email', 'vk', '_token']);

        foreach($data as $d){

            if(ContactsChecker::check($d)){
                return response('FAILED');
            }
        }



        return $next($request);
    }
}
