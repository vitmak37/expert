<?php

namespace App\Http\Middleware;

use Closure;

class HasAssignedCard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->check() || !auth()->user()->hasAssignedCard()) {
            return redirect()->back()
                ->with(['errorModal' => ['title' => 'Нет привязанной safecrow карты', 'message' => 'Для выполнения этого действия, вам нужна привязанная к аккаунту safecrow карта']]);
        }
        return $next($request);
    }
}
