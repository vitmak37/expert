<?php

namespace App\Http\Controllers;

use App\Complaint;
use App\Order;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $orders = Order::whereHas('complaints', function ($query) {$query->where('is_read', 0);})->orderBy('id', 'desc')->get();
        return view('admin.index', compact('orders'));
    }

    public function readComplaints ($orderId) {
        $order = Order::findOrFail($orderId);
        Complaint::markReadById($orderId);

        return redirect()->back()->with(['success' => 'Прочитано']);
    }
}
