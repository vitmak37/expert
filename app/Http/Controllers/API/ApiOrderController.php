<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;

class ApiOrderController extends Controller
{
    public function index(Request $request)
    {
        $limit = 20;
        $offset = $request->input('page') ?? 0;
        $orders = Order::limit($limit)->offset($offset)->orderBy('id', 'desc')->where('status', 'auction')->get();
        return $orders;
    }
}
