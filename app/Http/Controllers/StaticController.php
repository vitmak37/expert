<?php

namespace App\Http\Controllers;

class StaticController extends Controller
{
    public function about()
    {
        return view('static.about');
    }

    public function rules()
    {
        return view('static.rules');
    }

    public function faq()
    {
        //return redirect()->back()->with(['errorModal' => ['title' => 'Ошибка доступа', 'message' => 'Данный раздел находится в разработке']]);
        return view('static.faq');
    }

    public function userAgreement()
    {
        return view('static.user-agreement');
    }

    public function privacyPolicy()
    {
        return view('static.privacy-policy');
    }

    public function pin()
    {
        return view('static.faq.pin');
    }

    public function moneyOut()
    {
        return view('static.faq.money-out');
    }

    public function account()
    {
        return view('static.faq.account');
    }

    public function cost()
    {
        return view('static.faq.cost');
    }

    public function what()
    {
        return view('static.faq.what');
    }

    public function freese()
    {
        return view('static.faq.freese');
    }

    public function fee()
    {
        return view('static.faq.fee');
    }

    public function payment()
    {
        return view('static.faq.payment');
    }
}