<?php

namespace App\Http\Controllers;

use App\Comment;
use App\File;
use App\Notification;
use App\Order;
use App\Respond;
use App\Services\SafecrowService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RespondController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'comment' => 'required|string',
            'order_id' => 'required|exists:orders,id',
            'discus' => 'nullable',
            'budget' => 'required_without:discus|numeric|min:100|nullable'
        ]);

        if (auth()->user()->addedRespond($request->input('order_id'))) {
            return redirect()->back()->with(['message' => 'Вы уже добавляли отклик к этому заказу']);
        }

        if (Order::findOrFail($request->input('order_id'))->status != Order::STATUS_AUCTION) {
            return redirect()->back()->with(['message' => 'Невозможно добавить отклик к заказу не в аукционе']);
        }

        $userId = auth()->user()->id;

        $respond = Respond::create([
            'user_id' => $userId,
            'order_id' => $request->input('order_id'),
            'status' => 'pending',
            'budget' => $request->discus ? 0 : $request->budget,
            'comment' => $request->input('comment')
        ]);

        return redirect()->route('orders.show', ['id' => $request->input('order_id')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function comment(Request $request, Respond $respond)
    {
        $rules = ['comment' => 'required_without:files|string|nullable', 'files' => 'nullable|array'];

        if ($request->has('files')) {
            foreach (range(0, count($request->file('files'))) as $num) {
                $rules['files.' . $num] = 'file';
            }
        }

        $this->validate($request, $rules);

        if ($request->has('intermediate')) {
            $commentType = 'intermediate';
        } elseif ($request->has('final')) {
            $this->validate($request, ['files' => 'required']);
            $commentType = 'final';
        }

        $comment = Comment::create([
            'user_id' => auth()->user()->id,
            'responds_id' => $respond->id,
            'status' => 'accepted',
            'type' => $commentType ?? 'comment',
            'comment' => $request->comment
        ]);

        if (isset($commentType) && $commentType == 'final') {
            $respond->order->update(['status' => 'pending']);
        }


        if ($request->has('files')) {
            $files = [];
            foreach ($request->file('files') as $file) {
                $path = $file->store('public/files');
                $f = File::create([
                    'path' => $path,
                    'original_name' => $file->getClientOriginalName()
                ]);
                $files[] = $f->id;
            }
            $comment->files()->attach($files);
        }


        return redirect()->back();
    }

    public function selectExecutor($orderId, $respondId)
    {
        $order = Order::findOrFail($orderId);
        $respond = Respond::findOrFail($respondId);

        if (!auth()->user()->id == $order->user->id) {
            return redirect()->back()->with(['message' => 'Невозможно изменить статус: вы не являетесь создателем лота']);
        }

        if ($order->status != Order::STATUS_AUCTION) {
            return redirect()->back()->with(['message' => 'Невозможно изменить статус: лот ужу не на аукционе']);
        }

        $order->setExecutor($respond->id);

        return redirect()->back()->with(['message' => 'Статус успешно изменен']);
    }

    public function choose(Request $request, Respond $respond)
    {
        if (!$request->user()->id == $respond->order->user->id) {
            return redirect()->back()->with(['message' => 'К сожалению, у вас недостаточно прав для выбора исполнителя данного заказа']);
        }

        if ($respond->order->status != Order::STATUS_AUCTION) {
            return redirect()->back()->with(['message' => 'Данному заказу больше нельзя выбрать исполнителя']);
        }

        $respond->order->setExecutor($respond->id);

        return redirect()->back()->with(['message' => 'Исполнитель выбран']);
    }

    public function updateCost(Request $request, $id)
    {
        $this->validate($request, ['budget' => 'required|numeric|min:100']);
        $respond = Respond::findOrFail($id);
        $respond->update(['budget' => $request->budget]);
        return redirect()->back()->with(['successModal' => ['title' => 'Стоимость установлена', 'message' => 'Стоимость успешно установлена, подтвердите свое согласие с выполнением.']]);
    }

    public function accept(Request $request, $respondId)
    {

        // TODO S(!)olid
        $respond = Respond::findOrFail($respondId);
        if(auth()->user()->id == $respond->order->user_id && $respond->id == $respond->order->winned_respond && !$respond->executant_accepted) {
            return redirect()->back()->with(['errorModal' => ['title' => 'Исполнитель еще не принял условия!', 'message' => 'Мы заботимся о вас! Исполнитель может изменить стоимость в любой момент до своего согласия!']]);
        }
        if (auth()->user()->id == $respond->user_id) {
            $cards = implode(',', auth()->user()->getListOfUserCardIds(auth()->user()->getListOfUserCards()));
            $this->validate($request, ['card_id' => 'required|in:' . $cards]);
            $respond->update(['executant_accepted' => true, 'executant_card_id' => $request->card_id]);
        } else if (auth()->user()->id == $respond->order->user_id && $respond->id == $respond->order->winned_respond && $respond->executant_accepted) {
            $respond->update(['customer_accepted' => true]);
        } else {
            return redirect()->back()->with(['errorModal' => ['title' => 'Действие запрещено', 'message' => 'Вам нельзя так делать!']]);
        }

        if ($respond->executant_accepted && $respond->customer_accepted) {
            // Создаем заказ SC
            $sc = new SafecrowService();
            $scorder = $sc->createOrder($respond->order->id);
            // Привязываем карточки SC
            $sc->assignCardsToOrder($scorder, $respond->executant_card_id, $respond->user->sc_user_id);
            // Начинаем выполнение
            $respond->order->update(['status' => Order::STATUS_PAYMENT, 'sc_order_id' => $scorder]);
        }

        return redirect()->back()->with(['successModal' => ['title' => 'Ваше согласие принято!', 'message' => 'Уже скоро заказ должен быть исполнен!']]);
    }

    public function discard($respondId)
    {
        $respond = Respond::findOrFail($respondId);
        $respond->order->unsetExecutor();
        Notification::send($respond->user_id, 'Отклик снят с исполнения', route('orders.show', ['id' => $respond->order->id]));
        Notification::send($respond->order->user_id, 'Отклик снят с исполнения', route('orders.show', ['id' => $respond->order->id]));
        $respond->delete();
        return redirect()
            ->back()
            ->with(['successModal' => ['title' => 'Успешно удален отклик к заказу', 'message' => 'Отклик был удален, а уведомление отправлено исполнителю и заказчику']]);
    }
}
