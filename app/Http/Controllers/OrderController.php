<?php

namespace App\Http\Controllers;

use App\File;
use App\GuaranteeTerm;
use App\Notification;
use App\Order;
use App\Services\SafecrowService;
use App\Subject;
use App\UniqnessService;
use App\WorkType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Windstep\Safecrow\Facades\Safecrow;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::available();
        $columns = [];

        if (request()->has('q') && request('q') != null) {
            $orders = $orders->withSearchQuery(request('q'));
            $columns['q'] = request('q');
        }

        if (request()->has('s') && request('s') != null) {
            $subjectRequest = gettype(request('s')) == 'array' ? request('s') : array(request('s'));
            $orders = $orders->bySubjects($subjectRequest);
            $columns['s'] = request('s');
        }

        if (request()->has('w') && request('w') != null) {
            $workTypesRequest = gettype(request('w')) == 'array' ? request('w') : array(request('w'));
            $orders = $orders->byWorkTypes($workTypesRequest);
            $columns['w'] = request('w');
        }

        $orders = $orders
            ->orderBy('id', 'desc')
            ->paginate(8)
            ->appends($columns);

        $subjects = Subject::all();
        $workTypes = WorkType::all();

        return view('orders.index')
            ->with('orders', $orders)
            ->with('subjects', $subjects)
            ->with('workTypes', $workTypes);
    }

    public function show(Order $order)
    {
        if (request()->has('status') && $order->status == Order::STATUS_PAYMENT) {
            $scorder = Safecrow::findOrder($order->sc_order_id);
            if ($scorder->status == 'paid') {
                $order->update(['status' => 'underway']);
                Notification::send($order->winner->user_id, 'Заказ оплачен! Можете приступать к выполнению!', route('orders.show', ['id' => $order->id]));
            }
        }
        // Weired bug with non-updating order var itself.
        $order = Order::findOrFail($order->id);
        $paymentUrl = '';
        if (auth()->check() && auth()->user()->id == $order->user_id && $order->status == Order::STATUS_PAYMENT) {
            try {
                $paymentUrl = auth()->user()->getPaymentUrl($order);
            } catch (\Exception $e) {
                report($e);
            }
        }
        return view('orders.show')
            ->with('order', $order)
            ->with('winnerRespond', $order->winner)
            ->with('paymentUrl', $paymentUrl);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $subjects = Subject::all();
        $uniqueness_services = UniqnessService::all();
        $types = WorkType::all();
        return view('orders.create')
            ->with('subjects', $subjects)
            ->with('uniqueness_services', $uniqueness_services)
            ->with('types', $types);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'description' => 'required|string',
            'work_amount' => 'required|numeric|min:0',
            'deadline' => 'required|date_format:d/m/Y',
            'subject' => 'required|exists:subjects,id',
            'uniqness_service_id' => 'required|exists:uniqness_services,id',
            'uniqness_service_percent' => 'required|numeric|min:0|max:100',
            'budget' => 'required|numeric|min:100',
            'work_type' => 'required|exists:work_types,id',
            'files' => 'array|nullable',
            'files.*' => 'nullable|file'
        ]);

        $order = Order::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'work_amount' => $request->input('work_amount'),
            'deadline' => Carbon::createFromFormat('d/m/Y', $request->deadline)->startOfDay(),
            'uniqueness_service_id' => $request->input('uniqness_service_id'),
            'uniqueness_percentage' => $request->input('uniqness_service_percent'),
            'budget' => $request->input('budget'),
            'status' => 'auction',
            'user_id' => auth()->user()->id,
            'guarantee_terms_id' => GuaranteeTerm::first()->id
        ]);

        $order->subjects()->attach($request->input('subject'));
        $order->work_types()->attach($request->input('work_type'));

        if ($request->has('files')) {
            foreach ($request->allFiles()['files'] as $file) {
                $path = $file->store('public/files');
                $f = File::create(['path' => $path, 'original_name' => $file->getClientOriginalName()]);
                $order->files()->attach($f->id);
            }
        }

        return redirect()->route('orders.show', ['order' => $order->id])
            ->with(['successModal' => array('title' => 'Заказ успешно создан', 'message' => 'Осталось совсем чуть-чуть! Дождитесь подходящего отклика и нажмите на кнопку "выбрать".')]);
    }

    public function ban(Order $order)
    {
        $this->authorize('admin');
        $order->complaints()->update(['is_read' => 1]);
        $order->rejectAll();
        $order->ban();

        return redirect()->back()->with(['message' => 'Заказ заблокирован']);
    }

    public function personal()
    {
        $user = auth()->user();
        $orders = Order::personal();
        $columns = [];

        if (request()->has('q') && request('q') != null) {
            $orders = $orders->withSearchQuery(request('q'));
            $columns['q'] = request('q');
        }

        if (request()->has('s') && request('s') != null) {
            $subjectRequest = gettype(request('s')) == 'array' ? request('s') : array(request('s'));
            $orders = $orders->bySubjects($subjectRequest);
            $columns['s'] = request('s');
        }

        if (request()->has('w') && request('w') != null) {
            $workTypesRequest = gettype(request('w')) == 'array' ? request('w') : array(request('w'));
            $orders = $orders->byWorkTypes($workTypesRequest);
            $columns['w'] = request('w');
        }

        $orders = $orders
            ->paginate(15)
            ->appends($columns);

        $subjects = Subject::all();
        $workTypes = WorkType::all();

        return view('orders.personal')
            ->with('orders', $orders)
            ->with('subjects', $subjects)
            ->with('workTypes', $workTypes);
    }

    public function decline(Order $order)
    {
        $order->update(['status' => Order::STATUS_DECLINED]);
        return redirect()->back();
    }

    public function argue(Order $order)
    {
        $order->update(['status' => Order::STATUS_ARGUE]);
        return redirect()->back();
    }

    public function resume(Order $order)
    {
        $order->update(['status' => Order::STATUS_UNDERWAY]);
        return redirect()->back();
    }

    public function done(Order $order)
    {
        $response = Safecrow::closeOrder($order->sc_order_id, ['reason' => 'Успешное выполнение заказа.']);
        if ($response == true && isset($response->status) && $response->status == 'closed')
            $order->update(['status' => Order::STATUS_DONE, 'done_at' => Carbon::now()]);
        else {
            return redirect()->back()->with('errorModal', ['title' => 'Упс, что-то пошло не так', 'message' => 'Что-то пошло не так при попытке закрыть сделку.']);
        }
        return redirect()->back();
    }

    public function guarantee(Order $order)
    {
        if (!auth()->check() || !auth()->user()->id == $order->id) {
            return redirect()->back()->with('errorModal', ['title' => 'Упс, что-то пошло не так', 'description' => 'Вы не имеете право на это действие.']);
        }
        if ($order->done_at && !(Carbon::now()->diffInDays($order->done_at) > 20)) {
            $order->update(['status' => Order::STATUS_GUARANTEE]);
            Notification::send($order->winner->user_id, 'Заказ ' . $order->title . ' был возвращен на гарантию. Пожалуйста, доделайте', route('orders.show', ['id' => $order->id]));
            return redirect()->back()->with('successModal', ['title' => 'Мы вернули заказ на доработку', 'message' => 'Ждите:)']);
        }
        return redirect()->back()->with('errorModal', ['title' => 'Упс, что-то пошло не так', 'message' => 'Гарантийный срок уже вышел. Мы просим прощения:(']);
    }

    public function close(Request $request, Order $order)
    {
        if (!auth()->user()->id == $order->user_id) {
            return redirect()->back()->with(['errorModal' => ['title' => 'Недостаточно прав', 'message' => 'Для закрытия заказа вы должны быть его автором, верно?)']]);
        }
        if ($order->sc_order_id) {
            $sc = new SafecrowService();
            $sc->closeOrder($order->sc_order_id, $request->reason);
        }
        $order->update(['status' => Order::STATUS_BANNED, 'ban_reason' => $request->reason]);
        if ($order->winned_respond) {
            Notification::send($order->winner->user_id, 'Заказ был удален:( Можете перестать работать', route('orders.show', ['id' => $order->id]));
        }

        return redirect()->back()->with(['successModal' => ['title' => 'Заказ удален', 'message' => 'Вы успешно удалили свой заказ, но все равно возвращайтесь']]);
    }

    public function updateStatus(Order $order)
    {
        $scorder = Safecrow::findOrder($order->sc_order_id);
        if ($scorder->status == 'paid') {
            $order->update(['status' => 'underway']);
            Notification::send($order->winner->user_id, 'Заказ оплачен! Можете приступать к выполнению!', route('orders.show', ['id' => $order->id]));
        }

        return redirect()->back()->with(['successModal' => ['title' => 'Статус заказа проверен!', 'message' => 'Статус заказа проверен и соответствует статусу заказа на SafeCrow']]);
    }
}
