<?php

namespace App\Http\Controllers;

use App\AcademicDegree;
use App\AcademicTitle;
use App\CustomerProfile;
use App\Exceptions\SafecrowResponseException;
use App\ExecutantProfile;
use App\File;
use App\Http\Requests\UserUpdateRequest;
use App\Services\SafecrowService;
use App\Services\UserService;
use App\Specialization;
use App\Subject;
use App\User;
use App\WorkType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Windstep\Safecrow\Facades\Safecrow;
use Windstep\Safecrow\SafecrowApi;

class UserController extends Controller
{
    public function show(User $user)
    {
        return view('users.show')
            ->with('user', $user);
    }

    public function profile()
    {
        $user = auth()->user();
        $workTypes = WorkType::all();
        $academicTitles = AcademicTitle::all();
        $academicDegrees = AcademicDegree::all();
        $specializations = Specialization::all();

        return view('users.profile')
            ->with('user', $user)
            ->with('workTypes', $workTypes)
            ->with('academicTitles', $academicTitles)
            ->with('academicDegrees', $academicDegrees)
            ->with('specializations', $specializations);
    }

    public function update(UserUpdateRequest $request)
    {
        $user = auth()->user();
        try {
            UserService::update($user, $request->except(['_token']));
        } catch (ValidationException $e) {

            return redirect()->back()->withErrors($e->validator);
        } catch (\Exception $e) {
            return redirect()->back()->with(['errors', array('user' => __('messages.no_profile'))]);
        }
        return redirect()->back()->with(['status' => __('messages.success')]);
    }

    public function setType(Request $request)
    {
        $this->validate($request, ['profile_type' => 'required|in:executant,customer']);
        $user = auth()->user();

        $profile = $request->input('profile_type') == User::TYPE_CUSTOMER ? new CustomerProfile() : new ExecutantProfile();
        $profile->save();

        $user->type = $request->input('profile_type');
        if ($request->input('profile_type') == User::TYPE_CUSTOMER) {
            $user->customer_profile_id = $profile->id;
        } else {
            $user->executant_profile_id = $profile->id;
        }
        $user->save();

        return redirect()->back();
    }

    public function createSafecrowProfile(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'phone' => 'required|string',
            'name' => 'required|string'
        ]);


        $safecrow = new SafecrowService();
        try{
            $userId = $safecrow->getOrCreateUser($request->only(['email', 'phone', 'name']));
        } catch (SafecrowResponseException $e) {
            $resp = $e->getResponse();
            return redirect()->back()->withErrors($resp->errors);
        }

        auth()->user()->update(['sc_user_id' => $userId]);

        return redirect()->back()
            ->with(['successModal' => [
                'title' => 'Успешно создан аккаунт Safecrow!',
                'message' => 'Ваш аккаунт safecrow для нашего сервиса успешно создан! Теперь вы можете полноценно пользоваться нашим ресурсом. Внимание! Для создания заказа/добавления отклика потребуется привязать карту'
                ]]);
    }

    public function assignCard($id)
    {
        return redirect()->to(
            config('safecrow.api_url') . Safecrow::getAttachCardLink($id, ['redirect_url' => '127.0.0.1:8000/cards/attached'])
        );
    }

    public function updatepw(Request $request)
    {
        $user = auth()->user();

        if($user->password == Hash::make($request->oldpassword)) {
            $user->password = Hash::make($request->newpassword);
        } else {
            return redirect()->back()->withErrors(['oldpassword' => 'Старый пароль должен быть введен корректно']);
        }

    }

    public function uploadPhoto(Request $request)
    {
        $this->validate($request, ['avatar' => 'file|required']);
        $loc = $request->file('avatar')->store('public');
        $file = new File();
        $file->path = $loc;
        $file->original_name = $request->file('avatar')->getClientOriginalName();
        $file->save();
        $user = auth()->user();
        $user->avatar()->associate($file);
        $user->save();
        return redirect()->back()->with('successModal', ['title' => 'Фотография загружена', 'message' => 'Теперь у вас новый аватар!']);
    }
}
