<?php

namespace App\Http\Controllers;

use App\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function read(Notification $notification)
    {

        if ($notification->user->id !== auth()->user()->id) {
            return redirect()->back();
        }

        $notification->update(['read_at' => Carbon::now()]);
        return redirect()->back();
    }
}
