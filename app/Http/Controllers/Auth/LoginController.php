<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Поддерживаем вход по email и по username
     * @return string
     */
    public function username()
    {
        /**
         * @var $request Request
         */
        $request = app('request');
        if ($request->email !== '' && filter_var($request->email,FILTER_VALIDATE_EMAIL))
            return 'email';
        else
            return 'username';
    }

    /**
     * Редиректим куда нужно после авторизации
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    protected function authenticated(Request $request, $user)
    {
        return response()->json([
            'redirect' => $request->session()->pull('url.intended', $this->redirectTo)
        ]);
    }

    public function showLoginForm()
    {
        return redirect()->action('index');
    }
}
