<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\OAuthRequest;
use App\SocialUser;
use App\Traits\RegistersUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use SocialiteProviders\Manager\OAuth2\User;

class OAuthController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/';

    public function login(OAuthRequest $request)
    {
        return Socialite::with($request->provider)->redirect();
    }

    public function callback()
    {
        return view('oauth-response');
    }

    protected function getUser(OAuthRequest $request)
    {
        /**
         * @var $user User
         */
        $oauth_user = Socialite::with($request->provider)->user();
        $oauth_id = $oauth_user->getId();

        // Если есть юзер с привязанной соц. сетью, авторизуем его
        if ($social_user = SocialUser::where('social_id',$oauth_id)
            ->where('provider',$request->provider)
            ->first())
        {
            if ($user = $social_user->user) {
                $this->guard()->login($user);
                return $this->authorized($request,$user);
            }
        }

        //Пытаемся войти под Email'ом из соц. сети
        $email = $oauth_user->getEmail();
        if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
            $email = $oauth_user->accessTokenResponseBody['email'] ?? '';
        }
        if ($email && $user = $this->guard()->getProvider()->retrieveById($email)) {

            //Привязываем соц. сеть к юзеру
            SocialUser::create([
                'user_id' => $user->id,
                'social_id' => $oauth_id,
                'provider' => $request->provider
            ]);

            $this->guard()->login($user);
            return $this->authorized($request, $user);
        }

        //Отправляем на регистрацию
        $username = '';
        if ($email) {
            preg_match('/(.*)@(.*).(.*)$/',$email,$matches);
            $username = $matches[1];
        }

        $user = [
            'oauth_provider' => $request->provider,
            'oauth_id' => $oauth_id,
            'username' => $username,
            'email' => $email
        ];

        return response()->json([
            'new' => true,
            'user' => $user
        ]);
    }

    public function authorized(Request $request, \App\User $user)
    {
        return response()->json([
            'redirect' => $this->redirectPath()
        ]);
    }
}
