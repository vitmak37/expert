<?php
namespace App\Traits;

use App\Role;
use App\SocialUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

trait RegistersUsers
{
    use \Illuminate\Foundation\Auth\RegistersUsers;


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'roles_id' => Role::where('name', 'user')->first()->id
        ]);

        if(isset($data['oauth_id']))
        {
            SocialUser::create([
                'user_id' => $user->id,
                'social_id' => $data['oauth_id'],
                'provider' => $data['oauth_provider']
            ]);
        }

        return $user;
    }

    protected function registered(Request $request, $user)
    {
        return response()->json([
            'redirect' => $this->redirectTo
        ]);
    }
}