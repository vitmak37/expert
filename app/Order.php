<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUS_DRAFT = 'draft';
    const STATUS_AUCTION = 'auction';
    const STATUS_UNDERWAY = 'underway';
    const STATUS_PENDING = 'pending';
    const STATUS_ARGUE = 'argue';
    const STATUS_DECLINED = 'declined';
    const STATUS_DONE = 'done';
    const STATUS_GUARANTEE = 'guarantee';
    const STATUS_BANNED = 'banned';
    const STATUS_ACCEPTANCE = 'accepting';
    const STATUS_PAYMENT = 'payment';

    const COMMENTABLE_STATUSES = ['underway', 'pending', 'argue', 'declined', 'done', 'guarantee', 'accepting', 'payment'];
    const CLOSABLE_STATUSES = ['auction', 'accepting', 'payment'];

    protected $casts = [
        'deadline' => 'datetime'
    ];

    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function uniqness_service()
    {
        return $this->belongsTo(UniqnessService::class, 'uniqueness_service_id');
    }

    public function guarantee_terms()
    {
        return $this->belongsTo(GuaranteeTerm::class, 'guarantee_terms_id');
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'subject_orders', 'orders_id', 'subjects_id');
    }

    public function work_types()
    {
        return $this->belongsToMany(WorkType::class, 'work_types_orders', 'orders_id', 'work_types_id');
    }

    public function complaints()
    {
        return $this->hasMany(Complaint::class, 'order_id');
    }

    public function responds()
    {
        return $this->hasMany(Respond::class, 'order_id');
    }

    public function activeResponds()
    {
        return $this->hasMany(Respond::class, 'order_id')->whereIn('status', self::COMMENTABLE_STATUSES);
    }

    public function winner()
    {
        return $this->belongsTo(Respond::class, 'winned_respond');
    }

    public function files()
    {
        return $this->belongsToMany(File::class, 'orders_has_files', 'orders_id', 'files_id');
    }

    public function getBudgetAttribute($budget)
    {
        if ($budget == 0) {
            return 'По договоренности';
        } else {
            return $budget . ' руб.';
        }
    }

    public function scopeAvailable($q)
    {
        return $q->where('status', 'auction');
    }

    public function scopePersonal($q)
    {
        return $q->where('user_id', auth()->user()->id)->orWhereHas('winner', function($q){
            $q->where('user_id', auth()->user()->id);
        });
    }

    public function scopePersonalByUserId($q, $userId)
    {
        return $q->where('user_id', $userId)->orWhereHas('winner', function ($q) use ($userId) {
           $q->where('user_id', $userId);
        });
    }

    public function scopeWithSearchQuery($q, string $query)
    {
        return $q->where('title', 'LIKE', '%' . $query . '%');
    }

    public function scopeBySubjects($q, array $subjects)
    {
        return $q->whereHas('subjects', function ($query) use ($subjects) {
            $query->whereIn('id', $subjects);
        });
    }

    public function scopeByWorkTypes($q, array $workTypes)
    {
        return $q->whereHas('work_types', function ($query) use ($workTypes) {
            $query->whereIn('id', $workTypes);
        });
    }

    public function setExecutor($respondId)
    {
        $respond = Respond::findOrFail($respondId);
        $this->responds()->where('id', '<>', $respond->id)->update(['status' => 'rejected']);
        $this->responds()->where('id', $respond->id)->update(['status' => 'accepted']);
        $this->update(['status' => self::STATUS_ACCEPTANCE, 'winned_respond' => $respondId]);
    }

    public function unsetExecutor($respondId)
    {
        $respond = Respond::findOrFail($respondId);
        $this->responds()->where('id', '<>', $respond->update(['status' => 'pending']));
        $this->responds()->where('id', $respond->id)->update(['status' => 'rejected']);
        $this->update(['status' => self::STATUS_AUCTION, 'winned_respond' => null]);
    }

    public function ban()
    {
        return $this->update(['status' => 'banned', 'ban_reason' => 'Нарушение правил пользования сайта']);
    }

    public function rejectAll()
    {
        return $this->responds()->update(['status' => 'rejected']);
    }

    public function statusColor()
    {
        switch($this->status){
            case Order::STATUS_DRAFT:
                return 'gray';
                break;
            case Order::STATUS_UNDERWAY:
                return 'primary';
                break;
            case Order::STATUS_PENDING:
                return 'yellow';
                break;
            case Order::STATUS_DONE:
                return 'green';
                break;
            case Order::STATUS_AUCTION:
                return 'pink';
                break;
            case Order::STATUS_ARGUE:
                return 'red';
                break;
            case Order::STATUS_DECLINED:
                return 'red';
                break;
            case Order::STATUS_GUARANTEE:
                return 'orange';
                break;
            case Order::STATUS_BANNED:
                return 'gray-dark';
                break;
            case Order::STATUS_ACCEPTANCE:
                return 'blue';
                break;
            default:
                return 'blue';
                break;
        }
    }

    public function printableStatus()
    {
        switch($this->status){
            case Order::STATUS_DRAFT:
                return 'Черновик';
                break;
            case Order::STATUS_ACCEPTANCE:
                return 'Обсуждение работы';
                break;
            case Order::STATUS_UNDERWAY:
                return 'В работе';
                break;
            case Order::STATUS_PENDING:
                return 'Ожидает просмотра';
                break;
            case Order::STATUS_DONE:
                return 'Выполнено';
                break;
            case Order::STATUS_AUCTION:
                return 'В аукционе';
                break;
            case Order::STATUS_ARGUE:
                return 'Спор';
                break;
            case Order::STATUS_DECLINED:
                return 'Отклонено';
                break;
            case Order::STATUS_GUARANTEE:
                return 'На гарантии';
                break;
            case Order::STATUS_BANNED:
                return 'Заблокирован';
                break;
            case Order::STATUS_PAYMENT:
                return 'Оплата';
                break;
            default:
                return 'Непонятный статус';
                break;
        }
    }
}
