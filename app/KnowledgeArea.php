<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KnowledgeArea extends Model
{
    public function subjects()
    {
        return $this->hasMany(Subject::class);
    }
}
