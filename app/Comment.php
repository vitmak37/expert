<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $table = 'site_comments';

    protected $fillable = ['user_id', 'responds_id', 'status', 'type', 'comment'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function respond(){
        return $this->belongsTo(Respond::class, 'responds_id');
    }

    public function files(){
        return $this->belongsToMany(File::class, 'comments_has_file', 'comment_id', 'file_id');
    }
}
