<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialUser extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'social_id',
        'provider'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
