<?php

namespace App;

use App\Services\SafecrowService;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Orchid\Platform\Core\Models\User as BaseUser;

class User extends BaseUser
{
    const TYPE_CUSTOMER = 'customer';
    const TYPE_EXECUTANT = 'executant';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'roles_id',
        'permissions',
        'profile_type',
        'type',
        'sc_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('isBanned', function ($builder) {
            $builder->where('is_banned', false);
        });
    }

    public function site_roles()
    {
        return $this->belongsTo(Role::class, 'roles_id');
    }

    public function executant_profile()
    {
        return $this->belongsTo(ExecutantProfile::class, 'executant_profile_id');
    }

    public function customer_profile()
    {
        return $this->belongsTo(CustomerProfile::class, 'customer_profile_id');
    }

    public function avatar()
    {
        return $this->belongsTo(File::class, 'avatar_file_id');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    /**
     * Отображает оценки, которые проставил пользователь
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rated()
    {
        return $this->hasMany(Rating::class, 'user_id');
    }

    /**
     * Отображает только оценки, которые получил пользователь
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class, 'executant_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function complaints()
    {
        return $this->hasMany(Complaint::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'user_id');
    }

    public function responds()
    {
        return $this->hasMany(Respond::class, 'user_id');
    }

    public function social_user()
    {
        return $this->hasMany(SocialUser::class, 'user_id', 'id');
    }

    public function getAuthIdentifierName()
    {
        return 'email';
    }

    public function getAvatarUrlAttribute()
    {
        return $this->avatar ? Storage::url($this->avatar->path) : '/storage/img/avatar.png';
    }

    public function customerValidationParameters()
    {
        return [
            'username' => 'required|string|max:191',
        ];
    }

    public function executantValidationParameters()
    {
        return [
            'username' => 'required|string|max:191',
            'qualification' => 'required|string|max:191',
            'about' => 'required|string|nullable',
            'education' => 'required|string',
            'academic_title_id' => 'required|exists:academic_titles,id',
            'academic_degree_id' => 'required|exists:academic_degrees,id'
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getValidationParameters()
    {
        if ($this->type == self::TYPE_CUSTOMER) {
            return $this->customerValidationParameters();
        } else {
            if ($this->type == self::TYPE_EXECUTANT) {
                return $this->executantValidationParameters();
            }
        }

        throw new \Exception("User must be either customer or executant");
    }

    public function addedRespond($orderId)
    {
        return $this->responds()->where('order_id', $orderId)->count() > 0;
    }

    public function personalOrder($orderId)
    {
        $order = Order::findOrFail($orderId);
        if (($order->winner && $order->winner->user->id == $this->id) || $order->user->id == $this->id) {
            return true;
        }
        return false;
    }

    public function personalOrders()
    {
        return Order::where('user_id', auth()->user()->id)->orWhereHas('winner', function($q){
            $q->where('user_id', auth()->user()->id);
        })->get();
    }

    public function averageRatings()
    {
        return round($this->ratings()->average('score'), 0);
    }

    public function anyUnreadNotification()
    {
        return $this->notifications()->where('read_at', null)->count() > 0;
    }

    public function hasUnvalidatedAlert()
    {
        return true;
    }

    public function hasSafecrowAccount()
    {
        return $this->sc_user_id ? true : false;
    }

    public function hasAssignedCard()
    {
        $safecrow = new SafecrowService();
        return $safecrow->hasAssignedCard($this->sc_user_id);
    }

    public function getCardAssignUrl()
    {
        $safecrow = new SafecrowService();
        $safecrowCardLink = $safecrow->getCardUrl(auth()->user()->sc_user_id, ['redirect_url' => URL::current()]);
        return $safecrowCardLink;
    }

    public function getListOfUserCards()
    {
        $safecrow = new SafecrowService();
        $safecrowCardList = $safecrow->attachedCards(auth()->user()->sc_user_id);
        return $safecrowCardList;
    }

    public function getListOfUserCardIds($cards)
    {
        $list = [];
        foreach ($cards as $item) {
            $list[] = $item->id;
        }

        return $list;
    }

    //TODO Move this to somewhere illogical
    public function getPaymentUrl(Order $order)
    {
        $sc = new SafecrowService();
        return $sc->getPaymentUrl($order->sc_order_id, $order->id);
    }

}